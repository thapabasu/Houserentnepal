<?php include_once('includes/header.php'); ?>

<?php
if (isset($_POST['form-submit'])) {
    $to = "basuthapa41@gmail.com";
    $subject = "Form submission";
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['msg'];
    $headers .= $email . "\r\n";
    $headers .= $name . "\r\n";

    if (mail($to, $subject, $message, $headers)) {
        echo "Mail Sent. Thank you " . $name . ", we will contact you shortly.";
    } else {
        echo "Sorry. There was problem sending the mail.";
    }
}
?>

    <div class="main-body">
        <div class="container">
            <div class="col-md-6 google-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3531.998634716657!2d85.33926692759123!3d27.717328429874893!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1973934b9fc7%3A0xabadbaeea8ff376e!2sBhatkekopul-Kalopul+Rd%2C+Kathmandu+44600!5e0!3m2!1sen!2snp!4v1461855757843"
                        width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <form method="post" action="#" name="contact-form" class="contact-form">
                    <div class="row">
                        <div class="col-md-12">
                            <span>Your Name *</span>
                            <input type="text" name="name" required="" placeholder="Your Name"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <span>Your Email *</span>
                            <input type="email" name="email" required="" placeholder="Your Email"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span>Your Message *</span>
                            <textarea name="msg" placeholder="Your Message..." required=""></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" name="form-submit" value="Send" id="contact-us-submit"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.title = 'Contact Us-HouseRentNepal';
    </script>
<?php include_once('includes/footer.php'); ?>