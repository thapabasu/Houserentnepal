<?php  include_once('includes/header.php');?>
  

  <?php 
     $a=@$_GET['a'];
     $b=@$_GET['b'];
  ?>       


	<div class="clearfix"></div>
	<div class="main-body">
		<div class="container login-form">
			<?php if (@$_GET['msg'] <> 'success') { ?>
				<?php if (@$_GET['msg'] == 'invalid'){ ?>	
				<h2 class="text-center" style="padding: 30px 0; color: #C81C1C;"><i class="fa fa-warning"></i> Invalid account detail...</h2>
				<form class="form-horizontal" id="log-form" method="post" action="login-validation.php" name="form-signin">
					<div class="form-group">
		    			<label for="inputEmail3" class="col-sm-2 control-label">Email<span>*</span></label>
		    			<div class="col-sm-10">
		      				<input type="email" value="<?php echo $a ?>" class="form-control" placeholder="Email" required="" name="login_email" id="login_email">
		    			</div>
		  			</div>
		   			<div class="form-group">
		    			<label for="inputPassword3" class="col-sm-2 control-label">Password<span>*</span></label>
		    			<div class="col-sm-10">
		      				<input type="password" value="<?php echo $b ?>" class="form-control" name="login_pass" id="login_Pass" placeholder="Password" required="">
		    			</div>
		  			</div>
		  			<div class="form-group">
		    			<div class="col-sm-offset-2 col-sm-10">
		      				<button type="submit" class="btn btn-primary" name="submit"><i class="fa fa-user"></i> Sign in</button>
		    			</div>
		  			</div>
	 			</form>
			<?php } elseif (@$_GET['msg'] == 'pending') { ?>
				<h2 class="text-center" style="padding: 30px 0; color: #C81C1C;"><i class="fa fa-warning"></i> Account pending...<br>Please activate your account by submitting code we sent to your e-mail...</h2>
				<h5 class="text-center">If you haven't received the mail, please check the spam or junk folder.</h5>
				<form method="post" action="code-submit.php?a=<?php echo $a; ?>&b=<?php echo $b; ?>">	
			  		<div class="text-center col-md-6 col-md-offset-3" style="padding: 20px 0; "><input type="text" class="form-control" placeholder="Code" required="" name="sub-code-input"></div>
			  		<div class="text-center col-md-4 col-md-offset-4"><button type="submit" class="btn btn-primary" name="code-submit"><i class="fa fa-tags"></i> Submit Code</button></div>
			    </form>
			<?php	} else {?>
				<?php if(@$_GET['codemsg'] <> 'invalid') { ?>
					<?php if(@$_GET['codemsg'] <> 'valid') { ?>	
						<h2 class="text-center" style="padding: 30px;">Sign in with your account detail...</h2>
					<?php } else { ?>
						<h2 class="text-center" style="padding: 30px;"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Account activated...<br/>Sign in to ad free posts...</h2>
					<?php } ?>
						<form class="form-horizontal" id="log-form" method="post" action="login-validation.php" name="form-signin">
							<div class="form-group">
			    				<label for="inputEmail3" class="col-sm-2 control-label">Email<span>*</span></label>
			    				<div class="col-sm-10">
			      					<input type="email" value="<?php echo $a ?>" class="form-control" placeholder="Email" required="" name="login_email" id="login_email">
			    				</div>
			  				</div>
			   				<div class="form-group">
			    				<label for="inputPassword3" class="col-sm-2 control-label">Password<span>*</span></label>
			    				<div class="col-sm-10">
			      					<input type="password" value="<?php echo $b ?>" class="form-control" name="login_pass" id="login_Pass" placeholder="Password" required="">
			    				</div>
			    			</div>
			    			<div class="form-group">
			    				<div class="col-sm-offset-2 col-sm-10">
			      					<button type="submit" class="btn btn-primary" name="submit"><i class="fa fa-user"></i> Sign in</button>
			    				</div>
			  				</div>
			    		</form>
		    	<?php } else { ?>
		    		<h2 class="text-center" style="padding: 30px 0; color: #C81C1C;"><i class="fa fa-warning"></i> Invalid code...<br>Please check your mail and try again...</h2>
		    		<form method="post" action="code-submit.php?a=<?php echo $a; ?>&b=<?php echo $b; ?>">	
				  		<div class="text-center col-md-6 col-md-offset-3" style="padding: 20px 0; "><input type="text" class="form-control" placeholder="Code" required="" name="sub-code-input"></div>
				  		<div class="text-center col-md-4 col-md-offset-4"><button type="submit" class="btn btn-primary" name="code-submit"><i class="fa fa-tags"></i> Submit Code</button></div>
				   	</form>
				<?php } ?>
		    <?php } ?>
		<?php } else { ?>
			<h2 class="text-center" style="padding: 30px 0;"><i class="fa fa-thumbs-up"></i>Registration Successful.<br>Activate account by submitting code we sent to your mail...</h2>
			<h5 class="text-center">If you haven't recieved the mail, please check the spam or junk folder.</h5>
		    		<form method="post" action="code-submit.php?a=<?php echo $a; ?>&b=<?php echo $b; ?>">	
				  		<div class="text-center col-md-6 col-md-offset-3" style="padding: 20px 0; "><input type="text" class="form-control" placeholder="Code" required="" name="sub-code-input"></div>
				  		<div class="text-center col-md-4 col-md-offset-4"><button type="submit" class="btn btn-primary" name="code-submit"><i class="fa fa-tags"></i> Submit Code</button></div>
				   	</form>
		<?php } ?>
		
		 <div class="clearfix"></div>
			 <p class="text-center"><span class="blink_me" style="display: inline; opacity: 0.980147;">Note :</span> If you don't have an account, please register now.</p>
 			 <div class="text-center"><a href="registration-form.php"><button class="btn btn-primary btn-lg">Register now<i class="fa fa-caret-square-o-right" style="padding-left:5px;"></i> </button></a></div>
		 </div>
	 </div>
 <script type="text/javascript">
          (function blink() { 
            $('.blink_me').fadeOut(500).fadeIn(500, blink);
          })();
 </script>
 <div class="clearfix"></div>
 <?php $pg = @$_GET['msg']; ?>
 <?php $cm = @$_GET['codemsg'] ?>
 <script type="text/javascript">
 	var p = '<?php echo $pg; ?>';
 	var c = '<?php echo $cm; ?>'
 	if(p != 'success' && c != 'invalid')
 		document.title = 'Login-RentOnNepal';
 	else
 		document.title = 'Code Submit-RentOnNepal';
 </script>
 <?php include_once('includes/footer.php'); ?>