<?php 
      include_once('includes/header.php'); 
      include_once('db_connect.php');
      $query="SELECT * FROM `mainbanner` WHERE `id`='1'";
      $result= $con->query($query);
      $res=mysqli_fetch_array($result);
 ?>
 
<div class="container-fluid indexmainbanner" style="background-image: url('./admin/uploads/<?php echo $res['photo']; ?>'); background-repeat: no-repeat;"> 
  <div class="homeheader"/><?php echo $res['header']; ?></div>
  <div class="clearfix"></div>
  <div class="homesecheader"><?php echo $res['paragraph']; ?></div>
  <div class="clearfix"></div>
  <div class="homebuttext"><a href="login.php" target="_self"><button class="btn btn-primary btn-lg"><?php echo $res['buttontext']; ?></button></a></div>
</div>

<?php 
  if(isset($_POST['quickSearch'])){
    $type = $_POST['quickSelect'];
    die("<script>location.href = 'quicksearch.php?type=$type'</script>");
  }
 ?>

<div class="clearfix"></div>
    
<div class="container">
<div class="panel-body">
<div class="row">
  <div class="col-md-4">
    <form id="quickSearch" style="border-bottom-style: dashed;" method="post" action="">
      <label>Quick search via property type <i class="fa fa-search"></i></label>
      <select class="form-control" required name="quickSelect">
        <option></option>
        <optgroup label="Rent">
          <option value="roomonrent">Room</option>
          <option value="flatonrent">Flat</option>
          <option value="shutteronrent">Shutter</option>
          <option value="buildingonrent">Building</option>
        </optgroup>
        <optgroup label="Sale">                  
          <option value="shutteronsale">Shutter</option>
          <option value="buildingonsale">Building</option>
          <option value="bungalowonsale">Bungalow</option>
          <option value="appartmentonsale">Appartment</option>
        </optgroup>
        <optgroup label="Lease">
          <option value="landonlease">Land</option>
          <option value="buildingonlease">Building</option>
        </optgroup>
      </select>
      <button type="submit" form="quickSearch" class="btn btn-success" style="margin: 10px 0;" name="quickSearch"><i class="fa fa-search"></i> Search</button>
    </form>
  </div>
  <div class="col-md-8">
  </div>
</div>
        
    <!-- row starts -->
    <?php 
      $query="SELECT `post`.`id`, `post`.`validdate`, `post`.`photo`, `post`.`categories`, `post`.`sub_category`, `post`.`dates`, `post`.`location`, `post`.`title`, `post`.`description`, `post`.`price`, `post`.`post_type`, `users`.`full_name`, `users`.`address`, `users`.`email`, `users`.`contact` FROM `post`, `users` WHERE `users`.`id` = `post`.`user_id` AND `post`.`status` = '1' ORDER BY `post`.`id` DESC LIMIT 12";
      $data=$con->query($query);
      $dataCount = mysqli_num_rows($data); 
      if($dataCount == 0){
        include_once('includes/no-data.php');
      }else{ ?>
      <div class="row">
      <marquee><h3 style="font-family: Raleway;">Most Recent Posts</h3></marquee>
      <?php
        $i=1;
        while ($row=mysqli_fetch_assoc($data)){ ?>
        <!-- col-ms-3 starts starts -->
        <div class="col-md-3"> 
          <!-- panel-default starts -->
          <div class="panel panel-default">
            <a 
              href="" class="indexViewDetail" 
              data-toggle="modal" 
              data-target="#viewDetailModal" 
              data-postImage="<?php echo $row['photo']; ?>"
              data-category="<?php echo $row['categories']; ?>"
              data-subCategory="<?php echo $row['sub_category']; ?>"
              data-postedDate="<?php echo $row['dates']; ?>"
              data-postLocation="<?php echo $row['location']; ?>"
              data-postTitle = "<?= $row['title']; ?>"
              data-postPrice = "<?= $row['price']; ?>"
              data-postType = "<?= $row['post_type']; ?>"
              data-postDescription = "<?= $row['description']; ?>"
              data-postValidDate = "<?= $row['validdate']; ?>"
              data-userName = "<?= $row['full_name']; ?>"
              data-userAddress = "<?= $row['address'] ?>"
              data-userEmail = "<?= $row['email'] ?>"
              data-userContact = "<?= $row['contact'] ?>">
              <!-- panel-body-img starts -->
              <div class="panel-body-img">
                <?php if($row['photo']){ ?>
                  <img src="./agent/uploads/<?php echo $row['photo']; ?>" class="img-responsive img-thumbnail" />
                <?php }else{ ?>
                  <img src="./images/no-photo.jpg" class="img-responsive" />
                <?php } ?>
              </div>
              <!-- panel-body-img ends -->
            </a>
            <!-- panel-body-label starts -->
            <div class="panel-body-label">
              <?php $t = $row['post_type']; ?>
              <?php if($t == "provider") { ?>
                    <span style="font-family:'Arvo', serif; font-size: 15px;"><?php echo $row['sub_category']; ?> Available On <?=$row['location'] ?> For <?= $row['categories']; ?></span><br/>
                  <?php } else { ?>
                    <span style="font-family:'Arvo', serif; font-size: 15px;"><?php echo $row['sub_category']; ?> Needed On <?=$row['location'] ?> For <?= $row['categories']; ?></span><br/>
                  <?php } ?>
              <a  href="" class="indexViewDetail" 
                  data-toggle="modal" 
                  data-target="#viewDetailModal" 
                  data-postId="<?php echo $row['id']; ?>" 
                  data-postImage="<?php echo $row['photo']; ?>"
                  data-category="<?php echo $row['categories']; ?>"
                  data-subCategory="<?php echo $row['sub_category']; ?>"
                  data-postedDate="<?php echo $row['dates']; ?>"
                  data-postLocation="<?php echo $row['location']; ?>"
                  data-postTitle = "<?= $row['title']; ?>"
                  data-postPrice = "<?= $row['price']; ?>"
                  data-postType = "<?= $row['post_type']; ?>"
                  data-postDescription = "<?= $row['description']; ?>"
                  data-postValidDate = "<?= $row['validdate']; ?>"
                  data-userName = "<?= $row['full_name']; ?>"
                  data-userAddress = "<?= $row['address'] ?>"
                  data-userEmail = "<?= $row['email'] ?>"
                  data-userContact = "<?= $row['contact'] ?>"
                  style="color: yellow;"><h6>View Details</h6>
              </a>
            </div>
            <!-- panel-body-label ends -->
          </div>
          <!-- panel-default ends -->
        </div>
        <!-- col-md-3 ends -->
          <?php echo ($i % 4 == 0?'<div class="clearfix"></div>':''); ?>
          <?php $i++; } ?>
        </div>
    </div><!-- row ends -->
    <?php } ?>
  </div><!-- panel-body ends -->
</div><!-- container ends --> 
<div class="clearfix"></div>

<!-- view detail modal -->
<?php include_once('includes/modal.php'); ?>
<!-- view detail modal ended -->

<div class="clearfix"></div>
    
<script type="text/javascript">
  $(document).ready(function(){
      $(".homeheader").hide();
      $(".homesecheader").hide();
      $(window).load(function(){
        $(".homeheader").slideDown(1000);
        $(".homesecheader").toggle(2500);
      });
      $(".indexViewDetail").click(function(){
        var postImage = $(this).attr('data-postImage');
        if(postImage == ''){
          $(".modal-body img").attr("src",'./agent/uploads/no-photo.jpg');
        }else{
          $(".modal-body img").attr("src",'./agent/uploads/'+postImage);
        }
        var postCategory = $(this).attr('data-category');
        var postSubCategory = $(this).attr('data-subCategory');
        var postLocation = $(this).attr('data-postLocation');
        var postType = $(this).attr('data-postType');
        if(postType == 'provider'){
          $(".modal-header h4").html(postSubCategory+' Available On '+postLocation+' For '+postCategory);
        }else{
          $(".modal-header h4").html(postSubCategory+' Needed On '+postLocation+' For '+postCategory);
        }
        var postDate = $(this).attr('data-postedDate');
        $("#postDate").html(postDate);
        var postTitle = $(this).attr('data-postTitle');
        $("#postTitle").html(postTitle);
        var postPrice = $(this).attr('data-postPrice');
        $("#postPrice").html(postPrice);
        var postDescription = $(this).attr('data-postDescription');
        $("#postDescription").html(postDescription);
        var userName = $(this).attr('data-userName');
        $("#userName").html('Name : '+userName);
        var userAddress = $(this).attr('data-userAddress');
        $("#userAddress").html('Address : '+userAddress);
        var userContact = $(this).attr('data-userContact');
        $("#userContact").html('Contact : '+userContact);
        var userEmail = $(this).attr('data-userEmail');
        $("#userEmail").html('Email : '+userEmail);
        var validdate = $(this).attr('data-postValidDate');
        $("#validDate").html(validdate);
      });
   });
</script>
<?php include_once('includes/footer.php'); ?>