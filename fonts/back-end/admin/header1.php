<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <title>Admin Panel</title>
  <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css" media="all" />

  <link rel="stylesheet" href="../css/style.css" type="text/css" media="all" />
  
  <script src="../js/jquery-1.12.2.min.js"></script>
   <script src="../js/bootstrap.min.js"></script> 
   <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
</head>
<body>
<div id="header" class="container-fluid">
  <div class="shell" class="container-fluid">
    <div id="top" class="container-fluid">
      <h1>Agent Panel</h1>
      <div class="container-fluid" style="color: #ffffff; margin: 1% 0 0 80%;">
        <div>Welcome <strong>Agent</strong>
        <span>|</span>
        <a style="color: #ffffff;" href="../agent/log-out.php">Log out</a>
        </div>
      </div>
      <div id="navigation" class="container-fluid">
        <ul>
            <li><a href="admin.php" class="active"><span>Dashboard</span></a></li>
          <li><a href="sitesettings.php"><span>Site settings</span></a></li>
          <li><a href="agentpanel.php"><span>Agent Panel Settings</span></a></li>
         </ul>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>