<?php include_once ('../includes/header.php'); ?>
<?php include_once ('../../db_connect.php'); ?>
<?php 
	if(isset($_GET['msg']) && $_GET['msg']='update'){
	 	$edt_id = $_GET['edit_id'];
	 	$qry="select post.id,post.title,post.categories,post.description,post.location,users.contact,post.price from `post` ,`users` where post.user_id = users.id and post.id='$edt_id' order by post.id";
		$rlt=mysql_query($qry);
		$rw = mysql_fetch_array($rlt);
 	} 
		
 ?>					
<div class="mypublish-edit-body">
	<h2 class="text-center" style="font-family: 'Arvo',serif;"><i class="fa fa-edit"></i>Edit Your Contents...</h2>
	<form action="mypublish-update.php?update_id=<?php echo $rw['id'] ;?>" method="post">
	  <div class="form-group">
	    <label for="title-txt">Title</label>
	    <input type="text" class="form-control" name="title-txt" id="title-txt" placeholder="TItle for your post" required="" value="<?php echo $rw['title']; ?>">
	  </div>
	   <div class="form-group">
	    <label for="category-select">Category</label>
	    <select required="" class="form-control" name="category-select" id="category-select">
			<option><?php echo $rw['categories']; ?></option>
			<option>Room</option>
			<option>Flat</option>
			<option>Building</option>
			<option>Shutter</option>
		</select>
	  </div>
	  <div class="form-group">
	    <label for="location-txt">Location</label>
	    <input type="text" class="form-control" name="location-txt" id="location-txt" placeholder="Eg. New Baneshwor, Kathmandu" required="" value="<?php echo $rw['location']; ?>">
	  </div>
	  <div class="form-group">
	    <label for="price-txt">Price</label>
	    <input type="text" class="form-control" name="price-txt" id="price-txt" placeholder="Not compulsory" value="<?php echo $rw['price']; ?>">
	  </div>
	  <div class="form-group">
	    <label for="description-txt">Description</label>
	    <textarea class="form-control" name="description-txt" id="description-txt" required=""><?php echo $rw['description']; ?></textarea>
	  </div>
	  <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>
	</form>
</div>
<?php include_once('../includes/footer.php'); ?>