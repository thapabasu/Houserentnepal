<?php include_once('../includes/header.php'); ?>
<div class="addnew-body">
	<h2 class="text-center" style="font-family: 'Arvo',serif">Add New Informations</h2>
	<?php 
		if(@$_GET['msg']){
			$message=urldecode($_GET['msg']);
			if($message <> 'Successfully Uploaded...'){
				echo "<h3 id='m' class='text-center' style='color: #C81C1C;'><i class='fa fa-warning'></i>".$message."</h3>";
			}else{
				echo "<h3 id='m' class='text-center' style='color: #C81C1C;'><i class='fa fa-thumbs-up'></i>".$message."</h3>";
			}
		}
	 ?>

	 <div class="container press-button" style="width: 100%;">
	 	<div class="text-center" style="margin: 0 auto;">
	 		<button class="btn btn-primary btn-lg" id="owner-butt">Post As Provider</button>
	 		<button class="btn btn-primary btn-lg" id="seeker-butt">Post As Seeker</button>
	 	</div>
	 </div>
	<div id="add-new-publish">
		<form enctype="multipart/form-data" action="postvalidation.php" method="post" onsubmit="return validate();">
		  <div class="form-group">
		    <label for="title-txt">Title</label><span style="color: red;">*</span>
		    <input type="text" class="form-control" name="title-txt" id="title-txt" placeholder="TItle for your post" required="" maxlength="500" />
		  </div>
		  <div class="form-group">
		    <label for="location-txt">Location</label><span style="color: red;">*</span>
		    <input type="text" class="form-control" name="location-txt" id="location-txt" placeholder="Eg. New Baneshwor, Kathmandu" required="" maxlength="50">
		  </div>
		  <div class="form-group">
		    <label for="category-select">Category</label><span style="color: red;">*</span>
		    <select required="" class="form-control" name="category-select" id="category-select">
				<option></option>
				<option>Room</option>
				<option>Flat</option>
				<option>Building</option>
				<option>Shutter</option>
			</select>
		  </div>
		  <div class="form-group">
		    <label for="description-txt" cols="10">Description</label><span style="color: red;">*</span>
		    <textarea class="form-control" name="description-txt" id="description-txt" required=""></textarea>
		  </div>
		   <div class="form-group">
		    <label for="price-txt">Price</label>
		    <input type="text" class="form-control" name="price-txt" id="price-txt" placeholder="Not compulsory" maxlength="10" />
		  </div>
		  <div class="form-group">
		    <label for="fileToUpload">Photo</label>
		    <input type="file" id="fileToUpload" name="fileToUpload" onclick="hideError();" />
		    <span class="imageerror" id="imageerror" style="color: red; font-size: 15px; font-family: 'Arvo',serif"></span>
		  </div>
		  <button type="submit" class="btn btn-primary" name="addnewsubmit" id="addnewsubmit">Submit</button>
		</form>
	</div>
	


	<div id="add-new-require">
		<form enctype="multipart/form-data" action="postvalidation.php" method="post" onsubmit="return validate();">
		  <div class="form-group">
		    <label for="title-txt">Title</label><span style="color: red;">*</span>
		    <input type="text" class="form-control" name="title-txt" id="title-txt" placeholder="TItle for your post" required="" maxlength="500" />
		  </div>
		  <div class="form-group">
		    <label for="location-txt">Location</label><span style="color: red;">*</span>
		    <input type="text" class="form-control" name="location-txt" id="location-txt" placeholder="Eg. New Baneshwor, Kathmandu" required="" maxlength="50">
		  </div>
		  <div class="form-group">
		    <label for="category-select">Category</label><span style="color: red;">*</span>
		    <select required="" class="form-control" name="category-select" id="category-select">
				<option></option>
				<option>Room</option>
				<option>Flat</option>
				<option>Building</option>
				<option>Shutter</option>
			</select>
		  </div>
		  <div class="form-group">
		    <label for="description-txt" cols="10">Description</label><span style="color: red;">*</span>
		    <textarea class="form-control" name="description-txt" id="description-txt" required=""></textarea>
		  </div>
		   <div class="form-group">
		    <label for="price-txt">Price Range</label>
		    <input type="text" class="form-control" name="price-txt" id="price-txt" placeholder="Not compulsory" maxlength="10" />
		  </div>
		  <button type="submit" class="btn btn-primary" name="addnewsubmitseek" id="addnewsubmitseek">Submit</button>
		</form>
	</div>
</div>
<script type="text/javascript">
					function hideError(){
						$("#imageerror").hide();
					}
					$("#imageerror").hide();
					function validate(){
						var file_size = $('#fileToUpload')[0].files[0].size;
						if(file_size > 5242880){
							$("#imageerror").html("<i class='fa fa-warning'></i> File size exceed, only 5 mb maximum file size allowed.");
							$("#imageerror").show();
							return false;
						}
						var file_extension=$("#fileToUpload").val();
						if(file_extension.match(".jpeg$")==".jpeg" || file_extension.match(".gif$")==".gif" || file_extension.match(".GIF$")==".GIF" || file_extension.match(".JPEG$")==".JPEG" || file_extension.match(".JPG$")==".JPG" ||file_extension.match(".jpg$")==".jpg" || file_extension.match(".PNG$")==".PNG" ||file_extension.match(".png$")==".png"){
							return true;
						}else{
							$("#imageerror").html("<i class='fa fa-warning'></i> Invalid image type.Only jpg,jpeg,png and gif allowed.");
							$("#imageerror").show();
							return false;
						}
					} 		

					$("#add-new-publish").hide();
					$("#add-new-require").hide();

					$("#owner-butt").click(function(){
						$("#m").hide();
						$("#add-new-require").hide();
						$("#add-new-publish").slideDown(1000);
					});	

					$("#seeker-butt").click(function(){
						$("#m").hide();
						$("#add-new-publish").hide();
						$("#add-new-require").slideDown(1000);
					});			
				</script>
<?php
include_once ('../includes/footer.php');
?>