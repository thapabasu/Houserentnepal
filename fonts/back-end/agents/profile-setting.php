<?php
include_once ('../includes/header.php');
include_once ('../../db_connect.php');
?>


<?php $agent_id=$_SESSION['id']; ?>


<?php 
	$sql="SELECT password FROM `users` WHERE `users`.`id`='$agent_id'";
	$exe=mysql_query($sql);
	$ftch=mysql_fetch_assoc($exe);
	$ps=$ftch['password'];
?>



<div class="profile-setting text-center">
		<div style="display: inline-flex; margin: auto;" >	
			<div class="dropdown contact-dropdown" style="margin-right: 10px;">
				  <a  class="dropdown-toggle" style="font-family: 'Arvo', serif;cursor: pointer;"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><button class="btn btn-success">Contact <span class="caret"></span></button></a>
				<ul class="dropdown-menu contact-dropdown-menu">
	                <li><a id="addContactButton" style="font-family: Arial; cursor: pointer;" >Add New Contact</a></li>
	                <li><a id="changeContactButton" style="font-family: arial; cursor: pointer;">Delete Existing Contact</a></li>
              	</ul>
			</div>
			


			<div class="dropdown email-dropdown" style="margin-right: 10px;">
				  <a  class="dropdown-toggle" style="font-family: 'Arvo', serif;cursor: pointer;"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><button class="btn btn-success">Email <span class="caret"></span></button></a>
				<ul class="dropdown-menu email-dropdown-menu">
	                <li><a id="addEmailButton" style="font-family: Arial; cursor: pointer;" >Add New Email</a></li>
	                <li><a id="changeEmailButton" style="font-family: arial; cursor: pointer;">Delete Existing Email</a></li>
              	</ul>
			</div>
		




			<div>
				<button class="btn btn-success" id=changePassButton>Password</button>
			</div>
		</div>
	


		<?php if (@$_GET['msg-return'] == 'error') {
			echo "<h3 id='message'>Sorry your request was not completed due to some error<br/>Please try again.</h3>";
		} ?>


		<?php 
			if(@$_GET['msg-return'] =='add-contact-success'){
				echo "<h3 id='message'><i class='fa fa-thumbs-up'></i> New Contact Added Successfully.</h3>";
			} 
		?>


		<?php if (@$_GET['msg-return'] == 'del-contact-success') {
			echo "<h3 id='message'><i class='fa fa-thumbs-up'></i> Contact Deleted Successfully.</h3>";
		} ?>



		<?php if (@$_GET['msg-return'] == 'del-email-success') {
			echo "<h3 id='message'><i class='fa fa-thumbs-up'></i> Email Deleted Successfully.</h3>";
		} ?>




		<?php if (@$_GET['msg-return'] == 'active') {
			echo "<h3 id='message'><i class='fa fa-thumbs-up'></i> Successfully Added.</h3>";
		} ?>


		<?php if (@$_GET['msg-return'] == 'email_already_exists') {
			echo "<h3 id='message' style='color: #C81C1C; padding-top: 10px;'><i class='fa fa-warning'></i> Email Already Exists.</h3>";
		} ?>

		<?php if (@$_GET['msg-return'] == 'contact-already-exists') {
			echo "<h3 id='message' style='color: #C81C1C; padding-top: 10px;'><i class='fa fa-warning'></i> Contact Already Exists.</h3>";
		} ?>


		<?php if (@$_GET['msg-return'] == 'pass-changed') {
			echo "<h3 id='message' style='padding-top: 10px;'><i class='fa fa-thumbs-up'></i> Password Changed Successfully.</h3>";
		} ?>


	<div class="container" id="addContactDiv">
		<form method="post" action="profile-setting-validation.php?msg=<?php echo $agent_id ; ?>" class="text-center col-md-4 col-md-offset-4" id="add-contact-form">
			<div class="form-group">
				 <label for="addContact-txt"><h2>Add Contact</h2></label>
	    		 <input type="text" class="form-control" name="addContact-txt" id="addContact-txt" placeholder="" required="" maxlength="15" minlength="10">
	    		 <span id="contact-error" style="color: #C81C1C;" ></span>
			</div>
			<button type="submit" class="btn btn-primary" name="addContact" id="Contact">Submit</button>
		</form>
	</div>
	






	<div class="container" id="changeContactDiv">
		<form method="post" action="profile-setting-validation.php?msg=<?php echo $agent_id ; ?>" class="text-center col-md-4 col-md-offset-4">
			 <label for="changeContact-txt"><h2>Delete Contact</h2></label><br/>
			<?php 
			 	$query="SELECT `contact` FROM `users` WHERE `id` = '$agent_id'";
			 	$row=mysql_fetch_array(mysql_query($query));
			 	$query1="SELECT `contact` FROM `user-additional-contact` WHERE `user_id` = '$agent_id'";
				$exe=mysql_query($query1);
			?>
			<div class="form-group">
  				<label for="sel1">Select Contact</label>
  				<select class="form-control" id="sel1" required="" name="delete_contact">
  					<option></option>
  					<?php while ( $row1=mysql_fetch_array($exe)) { ?>
     	 				<option><?php echo $row1[0]; ?></option>
     				<?php } ?>
    			</select>
			</div>
			<button type="submit" class="btn btn-primary" name="changeContact" id="Contact">Delete</button>
		</form>
	</div>
	



	


	<div class="container" id="addEmailDiv">
			<form method="post" action="profile-setting-validation.php?msg=<?php echo $agent_id; ?>" class="text-center col-md-4 col-md-offset-4">
				<div class="form-group">
					 <label for="addEmail-txt"><h2>Add Email</h2></label>
		    		 <input type="email" class="form-control" name="addEmail-txt" id="addEmail-txt" placeholder="" required="" maxlength="50">
				</div>
				<button type="submit" class="btn btn-primary" name="addEmail" id="addEmail">Submit</button>
			</form>
	</div>
	

	<?php if (@$_GET['msg-return'] == 'pending') { ?>
		<div class="container" id="submitCodeDiv">
			<?php $email=base64_decode(urldecode(@$_GET['m'])); ?>
			<?php $el=urlencode(base64_encode($email)); ?>
			<?php if (@$_GET['status'] == 'not-active') { ?>
				<?php echo "<h3 style='color: #C81C1C; padding-top: 10px;'><i class='fa fa-warning'></i> Invalid Code.</h3>" ?>
			<?php } else {?>
				<?php echo "<h3>Please submit code sent to ".$email." for validation.</h3>" ?>
			<?php } ?>
				<form method="post" action="profile-setting-validation.php?msg=<?php echo $agent_id; ?>&e=<?php echo $el ?>" class="text-center col-md-4 col-md-offset-4">
					<div class="form-group">
						<input type="text" class="form-control" name="sub-code-txt" id="sub-code-txt" placeholder="" required="">
					</div>
					<button type="submit" class="btn btn-primary" name="codeSubmit" id="codeSubmit">Submit</button>
				</form>
		</div>
	<?php } ?>

	




	<div class="container" id="changeEmailDiv">
		<form method="post" action="profile-setting-validation.php?msg=<?php echo $agent_id ; ?>" class="text-center col-md-4 col-md-offset-4">
			 <label for="deleteContact-txt"><h2>Delete Email</h2></label><br/>
			<?php 
			 	$query="SELECT `email` FROM `users` WHERE `id` = '$agent_id'";
			 	$row=mysql_fetch_array(mysql_query($query));
			 	$query1="SELECT `email` FROM `user-additional-email` WHERE `user_id` = '$agent_id'";
				$exe=mysql_query($query1);
			?>
			<div class="form-group">
  				<label for="sel1">Select Email</label>
  				<select class="form-control" id="sel1" required="" name="delete_email">
  					<option></option>
  					<?php while ( $row1=mysql_fetch_array($exe)) { ?>
     	 				<option><?php echo $row1[0]; ?></option>
     				<?php } ?>
    			</select>
			</div>
			<button type="submit" class="btn btn-primary" name="deleteEmail" id="deleteEmail">Delete</button>
		</form>
	</div>
	



	


	<div class="container" id="addChangePassDiv">
		<form class="text-center col-md-4 col-md-offset-4" method="post" action="profile-setting-validation.php?msg=<?php echo $agent_id; ?>" style="margin-top: 30px; margin-bottom: 10px;" id="change-pass">
			<div class="form-group">
				 <label for="ChangePass-txt"><h2>Change Password</h2></label>
			</div>
			<div class="form-group">
				<label for="CurrentPass-txt">Current Password</label><span style="color: red;">*</span>
				<div>
					<input type="password" class="form-control" name="CurrentPass-txt" id="CurrentPass-txt" placeholder="" required="">
					 <span  id="current-password-error" style="color: #C81C1C;"></span>
				</div>
			</div>
			<div class="form-group">
				<label for="NewPass-txt">New Password</label><span style="color: red;">*</span>
				<div>
					<input type="password" class="form-control" name="NewPass-txt" id="NewPass-txt" placeholder="" required="">
					<span  id="new-password-error" style="color: #C81C1C;"></span>
				</div>
			</div>
			<div class="form-group">
				<label for="ConfirmPass-txt">Confirm Password</label><span style="color: red;">*</span>
				<div>
					<input type="password" class="form-control" name="ConfirmPass-txt" id="ConfirmPass-txt" placeholder="" required="">
					<span  id="con-password-error" style="color: #C81C1C;"></span>
				</div>
			</div>
			<button type="submit" class="btn btn-primary" name="ChangePass" id="ChangePass">Submit</button>
		</form>
	</div>
</div>
<script type="text/javascript">
	$("#message").fadeOut(2000);
	$(".contact-dropdown").mouseenter(function(e) {
  		if ($(e.ralatedTraget).prop('class') != 'contact-dropdown-menu') {
 	  		$(".contact-dropdown-menu").stop().slideDown(500);
   		}
	});
	$(".contact-dropdown").mouseleave(function(e) {
   		if ($(e.ralatedTraget).prop('class') != 'contact-dropdown-menu') {
     		$(".contact-dropdown-menu").stop().slideUp(500);
  		 }
	});
	$(".email-dropdown").mouseenter(function(e) {
  		if ($(e.ralatedTraget).prop('class') != 'email-dropdown-menu') {
 	  		$(".email-dropdown-menu").stop().slideDown(500);
   		}
	});
	$(".email-dropdown").mouseleave(function(e) {
   		if ($(e.ralatedTraget).prop('class') != 'email-dropdown-menu') {
     		$(".email-dropdown-menu").stop().slideUp(500);
  		 }
	});
	
	
	$("#addContactDiv").hide();
	$("#changeContactDiv").hide();
	$("#addEmailDiv").hide();
	$("#addChangePassDiv").hide();
	$("#changeEmailDiv").hide();
	

	$("#addContactButton").click(function(){
		$("#submitCodeDiv").hide();
		$("#addContact-txt").focusout(function(){
			check_contact();
		});
		$("#addContactDiv").show();
		$("#addEmailDiv").hide();
		$("#addChangePassDiv").hide();
		$("#changeContactDiv").hide();
		$("#changeEmailDiv").hide();
		$("#add-contact-form").submit(function(event){
				if(check_contact() == true){
				}else{
				event.preventDefault();
				}
		});
	});
	

	$("#changeContactButton").click(function(){
		$("#submitCodeDiv").hide();
		
		$("#addContactDiv").hide();
		$("#addEmailDiv").hide();
		$("#addChangePassDiv").hide();
		$("#changeContactDiv").show();
		$("#changeEmailDiv").hide();
		
	});
	

	
	

	$("#addEmailButton").click(function(){
		$("#submitCodeDiv").hide();
		$("#addContactDiv").hide();
		$("#addEmailDiv").show();
		$("#addChangePassDiv").hide();
		$("#changeContactDiv").hide();
		$("#changeEmailDiv").hide();
		
	});
	

	$("#changeEmailButton").click(function(){
		$("#submitCodeDiv").hide();
		$("#addContactDiv").hide();
		$("#addEmailDiv").hide();
		$("#addChangePassDiv").hide();
		$("#changeContactDiv").hide();
		$("#changeEmailDiv").show();
		
	});
	

	
	$("#changePassButton").click(function(){
		$("#submitCodeDiv").hide();
		$("#addContactDiv").hide();
		$("#addEmailDiv").hide();
		$("#addChangePassDiv").show();
		$("#changeContactDiv").hide();
		$("#changeEmailDiv").hide();
		$("#current-password-error").hide();
		$("#new-password-error").hide();
		$("#con-password-error").hide();
		$("#CurrentPass-txt").focusout(function(){
			check_current_pwd();
		});
		$("#NewPass-txt").focusout(function(){
			check_new_pwd();
		});
		$("#ConfirmPass-txt").focusout(function(){
			check_con_pwd();
		});
		$("#change-pass").submit(function(event){
				if(check_new_pwd() == true && check_con_pwd() == true && check_current_pwd() == true ){
				}else{
				event.preventDefault();
				}
		});
	});



	function check_contact(){
			var Contact=$("#addContact-txt").val();
			if(isNaN(Contact)){
				$("#contact-error").html("<i class='fa fa-warning'></i> Invalid Contact.");
				$("#contact-error").show();
				return false;
			}else{
				$("#contact-error").hide();
				return true;
			}	
	}
	


	function check_new_pwd(){
				var pwd_length=$("#NewPass-txt").val().length;
				if(pwd_length<8){
					$("#new-password-error").html("<i class='fa fa-warning'></i> Password should have atleast 8 characters.");
					$("#new-password-error").show();
					return false;
				}else{
					$("#new-password-error").hide();
					return true;
				}
	}

	function check_con_pwd(){
			
				var newpwd=$("#NewPass-txt").val();
				var conpwd=$("#ConfirmPass-txt").val();
				if(newpwd != conpwd){
					$("#con-password-error").html("<i class='fa fa-warning'></i> Password do not match.");
					$("#con-password-error").show();
					return false;
				}else{
					$("#con-password-error").hide();
					return true;
				}
	}

	function check_current_pwd(){
				var cur_pwd=$("#CurrentPass-txt").val();
				var test="<?php echo $ps ;?>";
				if(cur_pwd != test){
					$("#current-password-error").html("<i class='fa fa-warning'></i> Invalid Current Password.");
					$("#current-password-error").show();
					return false;
				}else{
					$("#current-password-error").hide();
					return true;
				}
	}
</script>
<?php
include_once ('../includes/footer.php');
?>					