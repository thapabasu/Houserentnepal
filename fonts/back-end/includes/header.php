<?php 
session_start(); 
ini_set('session.gc_maxlifetime',1);
if($_SESSION['full_name'] == ''){
  header("Location: ../../login.php");
}
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RentOnNepal</title>
    <link rel="icon shortcut icon" href="../../images/icon.png" />
    <link href="../../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../css/own.css">
    <script src="../../js/jQuery/jquery-1.12.2.min.js"></script> 
    <script src="../../js/bootstrap.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../css/font-awesome.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
  </head>
  <body>
     <div class="container-fluid header">
      <div class="container navbar main-head" role="navigation">
        <div class="navbar-header">
        <a class="navbar-brand" href="../../index.php"><img style="padding-top: 8px;" class="logoimg img-responsive" src="../../images/logo.png" /></a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>

        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li class="active" style="background-color: #337AB7"><a style=" color: #ffffff;" href="index.php">Dashboard</a></li>
            <li><a href="addnew.php" style="" >Add New</a></li>
            <li><a href="profile-setting.php" style="" >Profile Setting</a></li><li><a href="mypublish.php" style="">My Publish</a></li>
            <li><a href="../agents/logout.php" style="">Logout</a></li>    
          </ul>
        </div>
      </div>
    </div>
 <div class="clearfix"></div>

   