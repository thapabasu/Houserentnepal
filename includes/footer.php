 <footer>
        <div class="clearfix"></div>
        <div class="container-fluid footer-part">

            <div class="container footer-logo">
              <a href="index.php">House<font style="color: #000000;font-weight: bolder;"><span>Rent</span></font>Nepal</a>
            </div>
            <div align="center" class="container" id="icon-footer">
                    <a class="facebook" href="https://www.facebook.com/adelyn.basu"><i class="fa fa-facebook-square" style="font-size: 2em; color: #3A5795;"></i></a>
                    <a class="twitter" href="https://twitter.com/RentOnNepal"><i class="fa fa-twitter-square" style="font-size: 2em; color: #55ACEE;"></i></a>
                    <a class="googleplus" href="https://plus.google.com/u/0/"><i class="fa fa-google-plus-square" style="font-size: 2em; color: #D95336;"></i></a>            
            </div>
        </div>
        <div class="container-fluid copyrights">
              <p> © 2016 Homerent. All Rights Reserved</p>
              <p>Powered by BASU THAPA</p>
        </div>   
</footer>
    <div class="clearfix"></div>
    </body>
</html>