<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="https://s0.wp.com/i/blank.jpg" />
    <title>RentOnNepal</title>
    <link rel="icon shortcut icon" href="images/icon.png" />
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/own.css">
    <script src="js/jQuery/jquery-1.12.2.min.js"></script> 
    <script src="js/jQuery/jquery-ui.min.js"></script> 
    <script src="js/bootstrap.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
    <link href='https://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
  </head>
  <body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


  <div class="loader"></div>
    <div class="container-fluid header">
      <div class="container navbar navbar-default main-head" role="navigation">
        <div class="navbar-header">
        <a class="navbar-brand" href="index.php"><img style="margin-top: -10px;" class="logoimg img-responsive" src="images/1.png"/></a>

        

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>

        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
        
          <ul class="nav navbar-nav">
            <li><a href="index.php"><h5>Home</h5></a></li>
            <li class="dropdown" id="category-dropdown">

              <a href="#" class="dropdown-toggle" id="services" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><h5>Services<span class="caret"></span></h5></a>
              <ul class="dropdown-menu" id="category-dropdown-menu" style="min-height: 100px;">
                <li><a href="sale.php" target="_self"><h5>Sale</h5></a></li>
                <li><a href="rent.php"  target="_self"><h5>Rent</h5></a></li>
                <li><a href="lease.php"  target="_self"><h5>Lease</h5></a></li>
              </ul>
            </li>  
            <li><a href="about-us.php"><h5>About Us</h5></a></li>
            <li><a href="contact-us.php"><h5>Contact Us</h5></a></li> 
            <li><a href="login.php"><h5>Login/Register</h5></a></li>           
            <li class="fb-like" data-href=
                "https://www.facebook.com/%E0%A4%95%E0%A5%8B%E0%A4%A0%E0%A4%BE-%E0%A4%96%E0%A4%BE%E0%A4%B2%E0%A5%80-%E0%A4%9B-1165081966856099/"
                 data-layout="button" data-action="like" data-show-faces="true" data-share="false" style="margin: 15px 15px 0;">
            </li>
            <li><a class="facebook" target="_blank" href="https://www.facebook.com/adelyn.basu">
                    <i class="fa fa-facebook-square" style="font-size: 2em; color: #3A5795;"></i>
                </a>
            </li>
            <li><a class="twitter" target="_blank" href="https://twitter.com/RentOnNepal">
                  <i class="fa fa-twitter-square" style="font-size: 2em; color: #55ACEE;"></i>
                </a>
            </li>
            <li><a class="googleplus" target="_blank" href="https://plus.google.com/u/0/">
                  <i class="fa fa-google-plus-square" style="font-size: 2em; color: #D95336;"></i>
                </a>
            </li>   
          </ul>
        </div>
      </div>
    </div>
<script type="text/javascript">
  $(window).load(function() {
    $(".loader").fadeOut("slow");
  });
  $("#category-dropdown").mouseenter(function(e){
      if ($(e.ralatedTraget).prop('id') != 'category-dropdown-menu') {
        $("#category-dropdown-menu").stop().slideDown("fast");
      }
  });
  $("#category-dropdown").mouseleave(function(e){
      if ($(e.ralatedTraget).prop('id') != 'category-dropdown-menu') {
        $("#category-dropdown-menu").stop().slideUp("fast");
      }
  });
  $(document).ready(function(){
    var path = window.location.pathname.split('/').pop();
    if(path == '')
      path = 'index.php';
    var target = $('#myNavbar li a[href="'+path+'"]');
    if(path == 'sale.php' || path == 'rent.php' || path == 'lease.php' || path == 'relocation.php' || path == 'location_search.php' || path == 'category-search.php' || path == 'quicksearch.php'){
      target = $('#category-dropdown');
      $('#category-dropdown a#services').css('background-color', '#082952');
      $('#category-dropdown a#services').css('color', '#fff');
    }
    target.css('background-color','#082952');
    target.css('color','#fff');
  });
</script>


 <div class="clearfix"></div>