<div class="modal fade" id="viewDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" style="font-weight: bold;" id="myModalLabel"></h4>
          </div>
          <div class="modal-body" style="height: 400px; overflow-y: auto;">
            <img src="" class="img img-responsive" style="margin: 0 auto; height: 300px; width: 300px;" />
            <div class="text-center" style="padding-top: 5px;">
              <section style="background-color: lightgrey;">
                <h4 style="font-family: 'Open Sans', sans-serif;">Posted On</h4>
                <p id="postDate"></p></br>
              </section>

              <h4 style="font-family: 'Open Sans', sans-serif;">Post Valid Up To</h4>
              <p id="validDate"></p></br>
              
              <section style="background-color: lightgrey;">
                <h4 style="font-family: 'Open Sans', sans-serif;">Title</h4>
                <p id="postTitle"></p></br>
              </section>
             
              <h4 style="font-family: 'Open Sans', sans-serif;">Description</h4>
              <p id="postDescription"></p></br>

              <section style="background-color: lightgrey;">
                <h4 style="font-family: 'Open Sans', sans-serif;">Price</h4>
                <p id="postPrice"></p></br>
              </section>
              
              <h4 style="font-family: 'Open Sans', sans-serif;">Agent Details</h4>
              <p id="userName"></p>
              <p id="userAddress"></p>
              <p id="userContact"></p>
              <p id="userEmail"></p>
  
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>