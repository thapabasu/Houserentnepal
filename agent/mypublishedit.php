<?php include_once ('includes/header.php'); ?>
<?php include_once ('../db_connect.php'); ?>
<?php 
	if(isset($_GET['msg']) && $_GET['msg']=='update'){
	 	$edt_id = $_GET['edit_id'];
	 	$qry="select post.title,post.categories,post.sub_category,post.description,post.location,post.price,post.post_type,post.validdate from `post` ,`users` where post.user_id = users.id and post.id='$edt_id' order by post.id";
		$rlt=mysqli_query($con, $qry);
		$rw = mysqli_fetch_array($rlt);
 	} 
		
 ?>					
<div class="main-body container">
	<h2 class="text-center" style="font-family: 'Arvo',serif;"><i class="fa fa-edit"></i>Edit Your Contents...</h2>
	<form enctype="multipart/form-data" action="mypublish-update.php?update_id=<?= $edt_id ?>" method="post" onsubmit="return validate();" style="margin-bottom: 10px;">
		  <input type="hidden" id="post-type-select" value="<?php echo $rw['post_type']; ?>">
		  <label for="title-txt">Title</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		  	<span class="input-group-addon"><i class="fa fa-bullhorn"></i></span>
		    <input type="text" class="form-control" name="title-txt" id="title-txt" placeholder="TItle for your post" required="" maxlength="500" value="<?= $rw['title']; ?>" />
		  </div>
		 <label for="location-txt">Location</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		  <span class="input-group-addon"><i class="fa fa-bank"></i></span>
		    <input type="text" class="form-control" name="location-txt" id="location-txt" placeholder="Eg. New Baneshwor, Kathmandu" required="" maxlength="50" value="<?= $rw['location']; ?>">
		  </div>
		  <label for="category-select">Category</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-reorder"></i></span>
		    <select required="" class="form-control" name="category-select" id="category-select">
				<?php if($rw['categories'] == 'Rent'){ ?>
					<option>Rent</option>
					<option class="buyOption">Buy</option>
					<option class="saleOption">Sale</option>
					<option>Lease</option>
				<?php }elseif($rw['categories'] == 'Sale'){ ?>
					<option class="saleOption">Sale</option>
					<option class="buyOption">Buy</option>
					<option>Rent</option>
					<option>Lease</option>
				<?php }elseif($rw['categories'] == 'Buy'){ ?>
					<option class="buyOption">Buy</option>
					<option class="saleOption">Sale</option>
					<option>Rent</option>
					<option>Lease</option>
				<?php }else{ ?>
					<option>Lease</option>
					<option class="buyOption">Buy</option>
					<option class="saleOption">Sale</option>
					<option>Rent</option>
				<?php } ?>
			</select>
			<span id="category-select-error"></span>
		  </div>
		  <label for="sub-category-select">Sub Category</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-reorder"></i></span>
		    <select required="" class="form-control" name="sub-category-select" id="sub-category-select">
				<?php if($rw['sub_category'] == 'Room') { ?>
					<option class="room">Room</option>
					<option class="flat">Flat</option>
					<option class="building">Building</option>
					<option class="bungalow">Bungalow</option>
					<option class="shutter">Shutter</option>
					<option class="apartment">Apartment</option>
					<option class="land">Land</option>
				<?php } elseif ($rw['sub_category'] == 'Flat') { ?>
					<option class="flat">Flat</option>
					<option class="room">Room</option>
					<option class="building">Building</option>
					<option class="bungalow">Bungalow</option>
					<option class="shutter">Shutter</option>
					<option class="apartment">Apartment</option>
					<option class="land">Land</option>
				<?php } elseif ($rw['sub_category'] == 'Building') { ?>
					<option class="building">Building</option>
					<option class="room">Room</option>
					<option class="flat">Flat</option>
					<option class="bungalow">Bungalow</option>
					<option class="shutter">Shutter</option>
					<option class="apartment">Apartment</option>
					<option class="land">Land</option>
				<?php } elseif ($rw['sub_category'] == 'Bungalow') { ?>
					<option class="bungalow">Bungalow</option>
					<option class="room">Room</option>
					<option class="flat">Flat</option>
					<option class="building">Building</option>
					<option class="shutter">Shutter</option>
					<option class="apartment">Apartment</option>
					<option class="land">Land</option>
				<?php } elseif ($rw['sub_category'] == 'Shutter') { ?>
					<option class="shutter">Shutter</option>
					<option class="room">Room</option>
					<option class="flat">Flat</option>
					<option class="building">Building</option>
					<option class="bungalow">Bungalow</option>
					<option class="apartment">Apartment</option>
					<option class="land">Land</option>
				<?php } elseif ($rw['sub_category'] == 'Apartment') { ?>
					<option class="apartment">Apartment</option>
					<option class="room">Room</option>
					<option class="flat">Flat</option>
					<option class="building">Building</option>
					<option class="bungalow">Bungalow</option>
					<option class="shutter">Shutter</option>
					<option class="land">Land</option>
				<?php } else { ?>
					<option class="land">Land</option>
					<option class="room">Room</option>
					<option class="flat">Flat</option>
					<option class="building">Building</option>
					<option class="bungalow">Bungalow</option>
					<option class="shutter">Shutter</option>
					<option class="apartment">Apartment</option>
				<?php } ?>
			</select>
		  </div>
		  <label for="validDate-txt" cols="10">Valid up to</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		  	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		  	<?php  
		  		$format = explode('-', $rw['validdate']);
		  	?>
		    <input type="date" class="form-control" name="validDate-txt" id="ui-datepicker" required="" value="<?= $format[1].'/'.$format[2].'/'.$format[0] ?>">
		  </div>
		  <label for="description-txt" cols="10">Description</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-newspaper-o"></i></span>
		    <textarea class="form-control" name="description-txt" id="description-txt" required=""><?= $rw['description'] ?></textarea>
		  </div>
		  
		  <div id="priceProvider">
			  <label for="price-txt">Price</label><br/>
			   <div class="input-group form-group">
			    <span class="input-group-addon">Rs.</span>
			    <input type="text" class="form-control" name="price-txt" id="price-txt" placeholder="Not compulsory"  onkeypress='return validateQty(event);' value=<?= $rw['price'] ?> />
			  </div>
			</div>

		  <button type="submit" class="btn btn-primary" name="editsubmit" id="editsubmit">Submit</button>
		</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var postType = $("#post-type-select").val();
		if(postType == 'provider'){
			$(".buyOption").attr('disabled','disabled');
			$(".saleOption").attr('disabled',false);
		}else{
			$(".saleOption").attr('disabled','disabled');
			$(".buyOption").attr('disabled',false);
		}
		var category = $("#category-select").val();
		if(category == 'Sale' || category == 'Buy' || category == 'Lease'){
			$(".room").attr('disabled','disabled');
			$(".flat").attr('disabled','disabled');
		}else{
			$(".room").attr('disabled',false);
			$(".flat").attr('disabled',false);
		}
		document.title = 'Edit Content-RentOnNepal';
		$( "#ui-datepicker" ).datepicker({
      		changeMonth: true,
      		changeYear: true
    	});
    	
    	$("#category-select").on('change', function(){
    		$("#sub-category-select").val('');
    		if($("#category-select").val() == 'Sale' || $("#category-select").val() == 'Buy' || $("#category-select").val() == 'Lease'){
				$(".room").attr('disabled','disabled');
				$(".flat").attr('disabled','disabled');
			}else{
				$(".room").attr('disabled',false);
				$(".flat").attr('disabled',false);
			}
    	});
	});
	
	function validateQty(event) {
    var key = window.event ? event.keyCode : event.which;
	if (event.keyCode == 8 || event.keyCode == 46
	 || event.keyCode == 37 || event.keyCode == 39) {
	    return true;
	}
	else if ( key < 48 || key > 57 ) {
	    return false;
	}
	else return true;
	};
</script>
<?php include_once('../includes/footer.php'); ?>