<div class="container-fluid">
    <footer class="container-fluid">
            <div class="clearfix"></div>
            <div class="container-fluid footer-part">

                <div class="container footer-logo">
                  <a href="index.php">rent<font style="color: #000000;font-weight: bolder;"><span>ON</span></font>nepal</a>
                </div>
                <div align="center" class="container" id="icon-footer">
                        <a class="facebook" href="#"><i class="fa fa-facebook-square" style="font-size: 2em; color: #3A5795;"></i></a>
                        <a class="twitter" href="#"><i class="fa fa-twitter-square" style="font-size: 2em; color: #55ACEE;"></i></a>
                        <a class="googleplus" href="#"><i class="fa fa-google-plus-square" style="font-size: 2em; color: #D95336;"></i></a>            
                </div>
            </div>
            <div class="container-fluid copyrights">
                  <p> © 2016 Homerent. All Rights Reserved</p>
            </div>   
    </footer>
</div>
    <!-- footer ended -->
    <div class="clearfix"></div>
    </body>
</html>