<?php 
session_start(); 
ini_set('session.gc_maxlifetime',1);
if($_SESSION['full_name'] == ''){
  header("Location: ../login.php");
}
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RentOnNepal</title>
    <link rel="icon shortcut icon" href="../images/icon.png" />
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../js/jQuery/sweetalert-master/dist/sweetalert.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/own.css">
    <script src="../js/jQuery/jquery-1.12.2.min.js"></script>
    <script src="../js/jQuery/jquery-ui/jquery-ui.min.js"></script>
    <script src="../js/jQuery/sweetalert-master/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="../js/jQuery/jquery-ui/jquery-ui.min.css"> 
    <script src="../js/bootstrap.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
  </head>
  <body>
      <div class="loader"></div>
     <div class="container-fluid header">
      <div class="container navbar main-head" role="navigation">
        <div class="navbar-header">
        <a class="navbar-brand" href="index.php"><img style="margin-top: -10px;" class="logoimg img-responsive" src="../images/1.png" /></a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>

        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li><a href="index.php" style="color: #777786;"><h5>Dashboard</h5></a></li>
            <li><a href="addnew.php" style="color: #777786;"><h5>Add New</h5></a></li>
            <li><a href="profile-setting.php" style="color: #777786;"><h5>Profile Setting</h5></a></li>
            <li><a href="mypublish.php" style="color: #777786;"><h5>My Publish</h5></a></li>
            <li><a href="logout.php" style="color: #777786;"><h5>Logout</h5></a></li>    
          </ul>
        </div>
      </div>
    </div>
 <div class="clearfix"></div>
 <script type="text/javascript">
   $(window).load(function() {
  $(".loader").fadeOut("slow");
  });
   $(document).ready(function(){
    var path = window.location.pathname.split('/').pop();
    if(path == '')
      path = 'index.php';
    var target = $('#myNavbar li a[href="'+path+'"]');
    if(path == 'mypublishedit.php'){
      target = $('#myNavbar li a[href="mypublish.php"]');
    }
    target.css('background-color','#082952');
    target.css('color','#fff');
  });
 </script>
   