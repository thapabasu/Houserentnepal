<?php
echo "<pre>";
include_once('../db_connect.php');
if (isset($_POST['addnewsubmit'])) {
    $response = '';
    $uid = $_SESSION['id'];
    $t = trim($_POST['title-txt']);
    $l = trim($_POST['location-txt']);
    $c = trim($_POST['category-select']);
    $sc = trim($_POST['sub-category-select']);
    $validDate = trim($_POST['validDate-txt']);
    $dateFormat = explode('/', $validDate);
    $m = $dateFormat[0];
    $d = $dateFormat[1];
    $y = $dateFormat[2];
    $validUpTo = $y . '-' . $m . '-' . $d;
    $d = trim($_POST['description-txt']);
    $p = trim($_POST['price-txt']);
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $image_name = $_FILES['fileToUpload']['name'];
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if (!empty($_FILES['fileToUpload']['name'])) {
        if (filesize($_FILES['fileToUpload']['tmp_name'])) {
            $response .= "";
        } else {
            $response .= "Only 2 mb file size allowed...";
        }
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check !== false) {
            $uploadOk = 1;
        } else {
            $uploadOk = 0;
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $deliminate = explode('.', $_FILES['fileToUpload']['name']);
            $newname = $deliminate[0] . time();
            $fullname = $newname . '.' . $deliminate[1];
            $target_file = $target_dir . $fullname;
            $image_name = $fullname;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 5242880) {
            $response .= "Sorry, your file is too large.Only 5mb file size allowed.";
            $uploadOk = 0;
        }
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG"
            && $imageFileType != "GIF"
        ) {
            $response .= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
            $response .= "Sorry, your file was not uploaded.";
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $dat = date("Y/m/d");
                $typ = "provider";
                if ($p != '') {
                    $query = "INSERT INTO post
						(user_id, dates, title, location, categories, sub_category, validdate, description, price, post_type, photo) 
						values 
						('$uid', '$dat', '$t','$l','$c', '$sc', '$validUpTo', '$d', '$p', '$typ', '$image_name')";
                } else {
                    $query = "INSERT INTO post
						(user_id, dates, title, location, categories, sub_category, validdate, description, post_type, photo) 
						values 
						('$uid', '$dat', '$t','$l','$c', '$sc', '$validUpTo', '$d', '$typ', '$image_name')";
                }

                if (mysqli_query($con, $query)) {
                    $response .= "Your post will be verified and uploaded soon. Thank You...";
                } else {
                    $response .= "Sorry, there was an error uploading your file...";
                }
            }
        }
    } else {
        echo "string";
        $dat = date("Y/m/d");
        $type1 = "provider";
        if ($p != '') {
            $query = "INSERT INTO post
						(user_id, dates, title, location, categories, sub_category, validdate, description, price, post_type) 
						values 
						('$uid', '$dat', '$t','$l','$c', '$sc', '$validUpTo', '$d', '$p', '$type1')";
        } else {
            $query = "INSERT INTO post
						(user_id, dates, title, location, categories, sub_category, validdate, description, post_type) 
						values 
						('$uid', '$dat', '$t','$l','$c', '$sc', '$validUpTo', '$d', '$type1')";
        }
        if (mysqli_query($con, $query)) {
            $response .= "Your post will be verified and uploaded soon. Thank You...";
        } else {
            $response .= "Sorry, there was an error uploading your file...";
        }
    }
}
if (isset($_POST['addnewsubmitseek'])) {
    $dat = date("Y/m/d");
    $response = '';
    $uid = $_SESSION['id'];
    $t = trim($_POST['title-txt']);
    $l = trim($_POST['location-txt']);
    $c = trim($_POST['category-select']);
    $sc = trim($_POST['sub-category-select']);
    $validDate = trim($_POST['validDate-txt']);
    $dateFormat = explode('/', $validDate);
    $m = $dateFormat[0];
    $d = $dateFormat[1];
    $y = $dateFormat[2];
    $validUpTo = $y . '-' . $m . '-' . $d;
    $d = trim($_POST['description-txt']);
    $p = trim($_POST['price-txt']);
    $dat = date("Y/m/d");
    $ty = "seeker";
    if ($p != '') {
        $query = "INSERT INTO post
				(user_id, dates, title, location, categories, sub_category, validdate, description, price, post_type) 
						values 
						('$uid', '$dat', '$t','$l','$c', '$sc', '$validUpTo', '$d', '$p', '$ty')";
    } else {
        $query = "INSERT INTO post
				(user_id, dates, title, location, categories, sub_category, validdate, description, post_type) 
						values 
						('$uid', '$dat', '$t','$l','$c', '$sc', '$validUpTo', '$d', '$ty')";
    }
    if (mysqli_query($con, $query)) {
        $response .= "Your post will be verified and uploaded soon. Thank You...";
    } else {
        $response .= "Sorry, there was an error uploading your post...";
    }

}
header("Location:addnew.php?msg=" . urlencode($response));
?>