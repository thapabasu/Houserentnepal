<?php include_once('includes/header.php'); ?>
<div class="container">
	<div class="index-body" style="margin-top: 120px; min-height: 400px;">
		<div id="welcome">
			<h2 class="text-center" style="padding: 30px 0; color: #4080b7;">Hi <?php echo $_SESSION['full_name'];?>, Welcome to agent panel.</h2><p>&nbsp;</p>
		</div>
		<div id="welcome-txt">
			<h3 class="text-center" style="font-family: 'Open Sans', sans-serif;">You can add new posts for free, can change your profile settings, view your posts, edit and delete your posts.</h3></br>
		</div>
		<div id="glad">
			<h3 class="text-center" style="font-family: 'Open Sans', sans-serif;">We're glad you're here !</h3>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#welcome").hide();
	$("#welcome-txt").hide();
	$("#glad").hide();
	$(document).ready(function(){
		$("#glad").slideDown(1600);
		$("#welcome-txt").slideDown(1600);
		$("#welcome").slideDown(1500);
	});
	document.title = 'Agent Panel-RentOnNepal';
</script>
<?php include_once('../includes/footer.php') ?>