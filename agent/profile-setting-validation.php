<?php include_once('../db_connect.php'); ?>

<?php if (isset($_POST['ChangePass'])): ?>
	<?php 	
		$agnt=@$_GET['msg'];
		$new_pass=trim($_POST['NewPass-txt']);
		$query="UPDATE `users` SET `users`.`password` = '$new_pass' WHERE `users`.`id`='$agnt'";
		mysqli_query($con, $query);
		$ps=$_POST['new-password'];	
		header("Location: profile-setting.php?msg-return=pass-changed");
	?>
<?php endif ?>


<?php if (isset($_POST['addContact'])): ?>
	<?php  
		$agnt=@$_GET['msg'];
		$new_contact=trim($_POST['addContact-txt']);
		$q="SELECT * FROM `users` WHERE `id` = '$agnt' AND `contact` = '$new_contact'";
		$count=mysqli_num_rows(mysqli_query($con, $q));
		$qu="SELECT * FROM `user-additional-contact` WHERE `user_id` = '$agnt' AND `contact` = '$new_contact'";
		$cnt=mysqli_num_rows(mysqli_query($con, $qu));
		if($count == 0 && $cnt == 0){
			$query="INSERT INTO `user-additional-contact` (contact,user_id) VALUES ('$new_contact','$agnt')";
			if(mysqli_query($con, $query)){
				header("Location: profile-setting.php?msg-return=add-contact-success");
			}else{
				header("Location: profile-setting.php?msg-return=error");
			}
		}else{
			header("Location: profile-setting.php?msg-return=contact-already-exists");
		}
 	?>
 <?php endif ?>



<?php if (isset($_POST['changeContact'])): ?>
	<?php  
		$agnt=@$_GET['msg'];
		$del_contact=trim($_POST['delete_contact']);
		$query="DELETE  FROM `user-additional-contact` WHERE `user_id` = '$agnt' AND `contact` = '$del_contact'";
		if(mysqli_query($con, $query)){
			header("Location: profile-setting.php?msg-return=del-contact-success");
		}else{
			header("Location: profile-setting.php?msg-return=error");
		}
 	?>
<?php endif ?>



<?php if (isset($_POST['deleteEmail'])): ?>
	<?php  
		$agnt=@$_GET['msg'];
		$del_email=trim($_POST['delete_email']);
		$query="DELETE  FROM `user-additional-email` WHERE `user_id` = '$agnt' AND `email` = '$del_email'";
		if(mysqli_query($con, $query)){
			header("Location: profile-setting.php?msg-return=del-email-success");
		}else{
			header("Location: profile-setting.php?msg-return=error");
		}
 	?>
<?php endif ?>











<?php if (isset($_POST['addEmail'])): ?>
	<?php  
		$agnt=@$_GET['msg'];
		$add_email=trim($_POST['addEmail-txt']);
		$qu="SELECT * FROM `user-additional-email` WHERE `user_id` = '$agnt' AND `email` = '$add_email'";
		$result=mysqli_query($con, $qu);
		$rw=mysqli_fetch_array($result);
		$que="SELECT * FROM `users` WHERE `id` = '$agnt' AND `email` = '$add_email'";
		$res=mysqli_query($con, $que);
		$cnt=mysqli_num_rows($result);
		$ct=mysqli_num_rows($res);
		$var=(rand() ."");
		if($cnt > 0 || $ct > 0){
			if($rw[4] == 'pending'){
				header("Location: profile-setting.php?msg-return=pending&m=".urlencode(base64_encode($add_email)));	
			}else{
				header("Location: profile-setting.php?msg-return=email_already_exists");
			}
		}else{
			$query="INSERT INTO  `user-additional-email` (email, user_id, code) VALUES ('$add_email', '$agnt', '$var')";
			if(mysqli_query($con, $query)){
				$to = $add_email;
				$subject = "Code submission";
				$message = "This is your code...".$var.".Thank You.";
				$headers = "From:rentonnepal.com" . "\r\n";
				mail($to,$subject,$message,$headers);
				header("Location: profile-setting.php?msg-return=pending&m=".urlencode(base64_encode($add_email)));
			}else{
				header("Location: profile-setting.php?msg-return=error");
			}
		}
 	?>
<?php endif ?>


<?php if (isset($_POST['codeSubmit'])): ?>
	<?php  
		$agnt=@$_GET['msg'];
		$c=trim($_POST['sub-code-txt']);
		$e=@$_GET['e'];
		$el=base64_decode(urldecode($e));
		$query="SELECT * FROM  `user-additional-email` WHERE `email` = '$el' AND `user_id` ='$agnt' AND `code` ='$c'";
		$result=mysqli_query($con, $query);
		$count=mysqli_num_rows($result);
		if($count != 0){
			$q="UPDATE `user-additional-email` SET `status` = 'active' WHERE `email` = '$el' AND `user_id` ='$agnt' AND `code` ='$c'";
			mysqli_query($con, $q);
			header("Location: profile-setting.php?msg-return=active");
		}else{
			header("Location: profile-setting.php?msg-return=pending&status=not-active&m=".urlencode(base64_encode($el)));
		}
 	?>
<?php endif ?>
