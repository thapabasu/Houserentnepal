<?php
include_once('includes/header.php');
include_once('../db_connect.php');
?>

<?php
if (isset($_POST['paginationgoto'])) {
    if (isset($_POST['paginationinput'])) {
        $t = trim($_POST['paginationinput']);
        header("Location: mypublish.php?page=" . $t);
    }
}
?>

<?php
$uid = $_SESSION['id'];
$page = @$_GET['page'];
if ($page == '' || $page == '1') {
    $page1 = 0;
} else {
    $page1 = ($page * 10) - 10;
}
$query = "select post.dates,post.id,post.title,post.categories,post.sub_category,post.description,post.price,post.location,post.post_type,post.validdate,users.contact from `post` ,`users` where post.user_id = users.id and post.user_id='$uid' and post.status='1' order by post.id desc limit $page1,10";
$result = mysqli_query($con, $query);
?>
<?php

if (isset($_GET['delete_id']) && !empty($_GET['delete_id'])) {
    $del_id = $_GET['delete_id'];
    $delete_query = "DELETE FROM post WHERE id='$del_id'";
    mysqli_query($con, $delete_query);
    header("Location: mypublish.php");
}

?>

    <div class="main-body container">
        <?php
        if (@$_GET['msg'] == 'success') {
            echo "<h5 class='text-center'>Successfully updated your data..</h5>";
        }
        if (@$_GET['msg'] == 'notsuccess') {
            echo "<h5 class='text-center'>Not updated due to some error! Please Try Again..</h5>";
        }
        ?>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Posted As</th>
                <th>Title</th>
                <th>Categories</th>
                <th>Sub Category</th>
                <th>Location</th>
                <th>Price</th>
                <th>Posted Date</th>
                <th>Valid Up To</th>
                <th>Content Control</th>
            </tr>
            <?php
            while ($row = mysqli_fetch_assoc($result)) {
                ?>
                <tr>
                    <td><?php echo $row['post_type']; ?></td>
                    <td><?php echo $row['title']; ?></td>
                    <td><?php echo $row['categories']; ?></td>
                    <td><?php echo $row['sub_category']; ?></td>
                    <td><?php echo $row['location']; ?></td>
                    <?php $price = $row['price'];
                    if ($price == '') { ?>
                        <td>Not Defined</td>
                    <?php } else { ?>
                        <td>Rs. <?= $price; ?></td>
                    <?php } ?>
                    <td><?php echo $row['dates']; ?></td>
                    <td><?php echo $row['validdate']; ?></td>
                    <td>
                        <a style="cursor: pointer;"
                           onclick="sweetAlert({
                                   title: 'Confirm',
                                   text: 'Are you sure to delete ?',
                                   type: 'warning',
                                   showCancelButton: true,
                                   confirmButtonText: 'Confirm',
                                   confirmButtonColor: '#D43F3A',
                                   cancelButtonColor: '#FFFFFF',
                                   cancelButtonText: 'Cancel'
                                   }, function(isConfirm){
                                   if(isConfirm){
                                   window.location = 'mypublish.php?delete_id=<?php echo $row['id']; ?>';
                                   }
                                   });"
                           title="delete">
                            <button class="btn btn-xs btn-danger"><i class="fa fa-trash" style="padding: 0 15px;"></i>
                            </button>
                        </a>
                        <a href="mypublishedit.php?msg=update&edit_id=<?php echo $row['id']; ?>" title="edit">
                            <button class="btn btn-xs btn-success"><i class="fa fa-edit" style="padding: 0 10px;"></i>
                            </button>
                        </a>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <div class="clearfix"></div>
    <div class="container-fluid footer-pagination" style="float: right;margin: 3% 8%;">
        <?php
        $extractdb = mysqli_query($con, "SELECT * FROM `post`WHERE `user_id`='$uid'");
        $count = mysqli_num_rows($extractdb);
        if ($count == 0) {
            $pagedata = 1;
        } else {
            $pagedata = ceil($count / 10);
        }

        $next = '';
        $previous = '';
        if ($page == '') {
            $next = 2;
        } else {
            $next = $page + 1;
        }
        if ($page > 1) {
            $pre = $page - 1;
        }
        if ($page == 0) {
            $x = 1;
        } else {
            $x = $page;
        } ?>
        <span style="font-style: italic;font-family: Georgia, serif;">Showing <?php echo $x; ?>
            of <?php echo $pagedata; ?></span>


        <?php if ($pagedata > 1 && $pagedata <= 5): ?>
            <?php if ($page == 0 || $page == 1 || $page == 2 || $page == 3 || $page == 4 || $page == 5): ?>
                <?php if ($page != '' && $page != 1): ?>
                    <a href="mypublish.php?page=<?php echo $pre; ?>">
                        <button class="btn btn-primary">Previous</button>
                    </a>
                <?php endif ?>
                <?php for ($a = 1; $a <= $pagedata; $a++) { ?>
                    <a href="mypublish.php?page=<?php echo $a; ?>">
                        <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $a; ?>&nbsp;&nbsp;</button>
                    </a>
                <?php } ?>
                <?php if ($page != $pagedata): ?>
                    <a href="mypublish.php?page=<?php echo $next; ?>">
                        <button class="btn btn-primary">Next</button>
                    </a>
                <?php endif ?>
            <?php endif ?>
        <?php endif ?>



        <?php if ($pagedata > 5): ?>
            <?php if ($page == 0 || $page == 1 || $page == 2 || $page == 3): ?>
                <?php if ($page != '' && $page != 1): ?>
                    <a href="mypublish.php?page=<?php echo $pre; ?>">
                        <button class="btn btn-primary">Previous</button>
                    </a>
                <?php endif ?>
                <?php for ($a = 1; $a <= 5; $a++) { ?>
                    <a href="mypublish.php?page=<?php echo $a; ?>">
                        <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $a; ?>&nbsp;&nbsp;</button>
                    </a>
                <?php } ?>
                <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                <?php if ($page != $pagedata): ?>
                    <a href="mypublish.php?page=<?php echo $next; ?>">
                        <button class="btn btn-primary">Next</button>
                    </a>
                <?php endif ?>
            <?php endif ?>


            <?php if ($page > 3): ?>
                <?php if ($page <= $pagedata - 2): ?>
                    <?php if ($page != '' && $page != 1): ?>
                        <a href="mypublish.php?page=<?php echo $pre; ?>">
                            <button class="btn btn-primary">Previous</button>
                        </a>
                    <?php endif ?>
                    <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                    <?php $var = $page - 2; ?>
                    <?php $variable = $page + 2; ?>
                    <?php for ($value = $var; $value <= $variable; $value++) { ?>
                        <a href="mypublish.php?page=<?php echo $value; ?>">
                            <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $value; ?>&nbsp;&nbsp;
                            </button>
                        </a>
                    <?php } ?>
                    <?php if ($page != $pagedata - 2): ?>
                        <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                    <?php endif ?>
                    <?php if ($page != $pagedata): ?>
                        <a href="mypublish.php?page=<?php echo $next; ?>">
                            <button class="btn btn-primary">Next</button>
                        </a>
                    <?php endif ?>
                <?php endif ?>
            <?php endif ?>


            <?php if ($page == $pagedata - 1): ?>
                <?php if ($page != '' && $page != 1): ?>
                    <a href="mypublish.php?page=<?php echo $pre; ?>">
                        <button class="btn btn-primary">Previous</button>
                    </a>
                <?php endif ?>
                <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                <?php $q = $page - 3; ?>
                <?php $y = $page + 1; ?>
                <?php for ($p = $q; $p <= $y; $p++) { ?>
                    <a href="mypublish.php?page=<?php echo $p; ?>">
                        <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $p; ?>&nbsp;&nbsp;</button>
                    </a>
                <?php } ?>
                <?php if ($page != $pagedata): ?>
                    <a href="mypublish.php?page=<?php echo $next; ?>">
                        <button class="btn btn-primary">Next</button>
                    </a>
                <?php endif ?>
            <?php endif ?>


            <?php if ($page == $pagedata): ?>
                <?php if ($page != '' && $page != 1): ?>
                    <a href="mypublish.php?page=<?php echo $pre; ?>">
                        <button class="btn btn-primary">Previous</button>
                    </a>
                <?php endif ?>
                <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                <?php for ($a = $pagedata - 4; $a <= $pagedata; $a++) { ?>
                    <a href="mypublish.php?page=<?php echo $a; ?>">
                        <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $a; ?>&nbsp;&nbsp;</button>
                    </a>
                <?php } ?>
            <?php endif ?>
        <?php endif ?>


        <?php if ($pagedata != '' && $pagedata != 1): ?>
            <div class="pull-right" style="margin: 3px 0 0 2px;">
                <form method="post" action="?">
                    <input type="number" min="1" max="<?php echo $pagedata; ?>" required="" name="paginationinput"/>
                    <input type="submit" value="GO To" name="paginationgoto">
                </form>
            </div>
        <?php endif ?>
    </div>
    <div class="clearfix"></div>
    <script type="text/javascript">
        document.title = 'My Publish-RentOnNepal';
    </script>
<?php
include_once('../includes/footer.php');
?>