<?php include_once('includes/header.php'); ?>
<div class="container main-body" style="min-height: 400px;">
	<h2 class="text-center" style="font-family: 'Arvo',serif">Add New Informations</h2>
	<?php 
		if(@$_GET['msg']){
			$message=urldecode($_GET['msg']);
			if($message <> 'Your post will be verified and uploaded soon. Thank You...'){
				echo "<h4 id='m' class='text-center' style='color: #C81C1C;'><i class='fa fa-warning'></i>".$message."</h4>";
			}else{
				echo "<h4 id='m' class='text-center' style='color: #16AC3E;'><i class='fa fa-thumbs-up'></i>".$message."</h4>";
			}
		}
	 ?>

	 <div class="container press-button" style="width: 100%;">
	 	<div class="text-center" style="margin: 0 auto;">
	 		<button class="btn btn-default btn-lg" id="owner-butt"><h4>Post As Provider</h4></button>
	 		<button class="btn btn-default btn-lg" id="seeker-butt"><h4>Post As Seeker</h4></button>
		</div>
		<h5 id="postType"></h5>
	 </div>
	
	<div id="add-new-publish" class="container" style="margin: 20px;">
		<form enctype="multipart/form-data" action="postvalidation.php" method="post" onsubmit="return validate();">
		  <label for="title-txt">Title</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		  	<span class="input-group-addon"><i class="fa fa-bullhorn"></i></span>
		    <input type="text" class="form-control" name="title-txt" id="title-txt" placeholder="TItle for your post" required="" maxlength="500" />
		  </div>
		 <label for="location-txt">Location</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		  <span class="input-group-addon"><i class="fa fa-bank"></i></span>
		    <input type="text" class="form-control" name="location-txt" id="location-txt" placeholder="Eg. New Baneshwor, Kathmandu" required="" maxlength="50">
		  </div>
		  <label for="category-select">Category</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-reorder"></i></span>
		    <select required="" class="form-control category-select" name="category-select">
				<option></option>
				<option>Sale</option>
				<option>Rent</option>
				<option>Lease</option>
			</select>
		  </div>
		  <label for="sub-category-select">Sub Category</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-reorder"></i></span>
		    <select required="" class="form-control sub-category-select" name="sub-category-select" >
				<option class="empty"></option>
				<option class="room">Room</option>
				<option class="flat">Flat</option>
				<option class="building">Building</option>
				<option class="bungalow">Bungalow</option>
				<option class="shutter">Shutter</option>
				<option class="apartment">Apartment</option>
				<option class="land">Land</option>
			</select>
		  </div>
		  <label for="validDate-txt" cols="10">Valid up to</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		  	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		    <input type="text" class="form-control" name="validDate-txt" id="ui-datepicker" required="">
		  </div>
		  <label for="description-txt" cols="10">Description</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-newspaper-o"></i></span>
		    <textarea class="form-control" name="description-txt" id="description-txt" required=""></textarea>
		  </div>
		  <label for="price-txt">Price</label><br/>
		   <div class="input-group form-group">
		    <span class="input-group-addon">Rs.</span>
		    <input type="text" class="form-control" name="price-txt" id="price-txt" placeholder="Not compulsory" onkeypress='return validateQty(event);'  />
		  </div>
		  <label for="fileToUpload">Photo</label>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-image"></i></span>
		    <input type="file" id="fileToUpload" name="fileToUpload" onclick="hideError();" />
		    <span class="imageerror" id="imageerror" style="color: red; font-size: 15px; font-family: 'Arvo',serif"></span>
		  </div>
		  <button type="submit" class="btn btn-primary" name="addnewsubmit" id="addnewsubmit">Submit</button>
		</form>
	</div>
	


	<div id="add-new-require" style="margin: 20px;">
		<form enctype="multipart/form-data" action="postvalidation.php" method="post" onsubmit="return validate();">
		  <label for="title-txt">Title</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		  	<span class="input-group-addon"><i class="fa fa-bullhorn"></i></span>
		    <input type="text" class="form-control" name="title-txt" id="title-txt" placeholder="TItle for your post" required="" maxlength="500" />
		  </div>
		 <label for="location-txt">Location</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		  <span class="input-group-addon"><i class="fa fa-bank"></i></span>
		    <input type="text" class="form-control" name="location-txt" id="location-txt" placeholder="Eg. New Baneshwor, Kathmandu" required="" maxlength="50">
		  </div>
		  <label for="category-select">Category</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-reorder"></i></span>
		    <select required="" class="form-control seek-category-select" name="category-select" >
				<option></option>
				<option>Buy</option>
				<option>Rent</option>
				<option>Lease</option>
			</select>
		  </div>
		  <label for="sub-category-select">Sub Category</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-reorder"></i></span>
		    <select required="" class="form-control sub-category-select" name="sub-category-select">
				<option class="empty"></option>
				<option class="room">Room</option>
				<option class="flat">Flat</option>
				<option class="building">Building</option>
				<option class="bungalow">Bungalow</option>
				<option class="shutter">Shutter</option>
				<option class="apartment">Apartment</option>
				<option class="land">Land</option>
			</select>
		  </div>
		  <label for="validDate-txt" cols="10">Valid up to</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		  	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		    <input type="date" class="form-control" name="validDate-txt" id="ui-datepicker1" required="">
		  </div>
		  <label for="description-txt" cols="10">Description</label><span style="color: red;">*</span>
		  <div class="form-group input-group">
		    <span class="input-group-addon"><i class="fa fa-newspaper-o"></i></span>
		    <textarea class="form-control" name="description-txt" id="description-txt" required=""></textarea>
		  </div>
		  <label for="description-txt" cols="10">Price</label><span style="color: red;">*</span>
		   <div class="input-group form-group">
		    <span class="input-group-addon">Rs.</span>
		    <input type="text" class="form-control" name="price-txt" id="price-txt" placeholder="Not compulsory" onkeypress='return validateQty(event);'  />
		  </div>
		  <button type="submit" class="btn btn-primary" name="addnewsubmitseek" id="addnewsubmitseek">Submit</button>
		</form>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$( "#ui-datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
    $( "#ui-datepicker1" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
});
function hideError(){
	$("#imageerror").hide();
}
$("#imageerror").hide();
function validate(){
	var file_size = $('#fileToUpload')[0].files[0].size;
	if(file_size > 5242880){
		$("#imageerror").html("<i class='fa fa-warning'></i> File size exceed, only 5 mb maximum file size allowed.");
		$("#imageerror").show();
		return false;
	}
	var file_extension=$("#fileToUpload").val();
	if(file_extension.match(".jpeg$")==".jpeg" || file_extension.match(".gif$")==".gif" || file_extension.match(".GIF$")==".GIF" || file_extension.match(".JPEG$")==".JPEG" || file_extension.match(".JPG$")==".JPG" ||file_extension.match(".jpg$")==".jpg" || file_extension.match(".PNG$")==".PNG" ||file_extension.match(".png$")==".png"){
		return true;
	}else{
		$("#imageerror").html("<i class='fa fa-warning'></i> Invalid image type.Only jpg,jpeg,png and gif allowed.");
		$("#imageerror").show();
		return false;
	}
}

$(document).ready(function(){
$("#add-new-publish").hide();
$("#add-new-require").hide();

$("#owner-butt").click(function(){
	$("#owner-butt").attr('class', 'btn-lg btn-primary');
	$("#seeker-butt").attr('class', 'btn-lg btn-default');
	$("#m").hide();
	$("#add-new-require").hide();
	$("#add-new-publish").slideDown(1000);
	$("#postType").html('Posting as a provider.');
});	

$("#seeker-butt").click(function(){
	$("#seeker-butt").attr('class', 'btn-lg btn-primary');
	$("#owner-butt").attr('class', 'btn-lg btn-default');
	$("#m").hide();
	$("#add-new-publish").hide();
	$("#add-new-require").slideDown(1000);
	$("#postType").html('Posting as a seeker.');
});	
$(".category-select").on('change', function(){
	$(".sub-category-select").val('');
	if(($(".category-select").val()) == 'Sale' || ($(".category-select").val()) == 'Lease' || ($(".category-select").val()) == 'Buy'){
		$(".room").attr('disabled', true);
		$(".flat").attr('disabled', true);
	}else{
		$(".room").attr('disabled', false);
		$(".flat").attr('disabled', false);
	}
});
$(".seek-category-select").on('change', function(){
	$(".sub-category-select").val('');
	if(($(".seek-category-select").val()) == 'Buy' || ($(".seek-category-select").val()) == 'Lease'){
		$(".room").attr('disabled', true);
		$(".flat").attr('disabled', true);
	}else{
		$(".room").attr('disabled', false);
		$(".flat").attr('disabled', false);
	}
});
document.title = 'Add New-RentOnNepal';
});		


function validateQty(event) {
    var key = window.event ? event.keyCode : event.which;
	if (event.keyCode == 8 || event.keyCode == 46
	 || event.keyCode == 37 || event.keyCode == 39) {
	    return true;
	}
	else if ( key < 48 || key > 57) {
	    return false;
	}
	else if ( key < 48 || key > 57) {
	    return false;
	}
	else return true;
	};
</script>
<?php
include_once ('../includes/footer.php');
?>