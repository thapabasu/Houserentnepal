<?php  include_once('includes/header.php');?>


<div class="main-body">


<?php if (@$_GET['msg'] == 'already-registered'): ?>
	<h2 class="text-center" style="padding: 30px 0; color: #C81C1C;"><i class="fa fa-warning"></i> Email you entered is already registered.</h2>
<?php endif ?>


<?php if (@$_GET['msg'] == 'notsuccess'): ?>
	<h2 class="text-center" style="padding: 30px 0; color: #C81C1C;"><i class="fa fa-warning"></i> Sorry, registration failed.<br/>Please try again.</h2>
<?php endif ?>


	<form class="form-horizontal" id="registration-form" method="post" action="register-form-insert.php" name="registration-form">
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label">Full Name<span>*</span></label>
			<div class="col-sm-8">
  				<input type="text" class="form-control" value="<?php echo @$_GET['f'] ?>" required="" name="full-name-txt" id="full-name-txt" maxlength="25">
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword3" class="col-sm-4 control-label">Address<span>*</span></label>
			<div class="col-sm-8">
  				<input type="text" name="address-txt" id="address-txt" value="<?php echo @$_GET['a'] ?>" required="" placeholder="eg. Chabel,Kathmandu" class="form-control" maxlength="50"/>
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword3" class="col-sm-4 control-label">Mobile No.<span>*</span></label>
			<div class="col-sm-8">
  				<input type="text" value="<?php echo @$_GET['c'] ?>" name="contact-txt" id="contact-txt" required="" class="form-control" maxlength="15" minlength="10" />
  				<span id="contact-error" style="color: #C81C1C;"></span>
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword3" class="col-sm-4 control-label">Email<span>*</span></label>
			<div class="col-sm-8">
  				<input type="email" value="<?php echo @$_GET['e'] ?>" name="email-txt" id="email-txt" required="" class="form-control" maxlength="50" />
				<span id="email-error" style="color: #C81C1C;"></span>
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword3" class="col-sm-4 control-label">Password<span>*</span></label>
			<div class="col-sm-8">
  				<input type="password" value="<?php echo @$_GET['p'] ?>" name="pwd-txt" id="pwd-txt" required="" placeholder="Password for signing in" class="form-control" /><span id="password-error" style="color: #C81C1C;" maxlength="20"></span>
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword3" class="col-sm-4 control-label">Confirm Password<span>*</span></label>
			<div class="col-sm-8">
  				<input type="password" value="<?php echo @$_GET['r'] ?>" name="retype-pwd-txt" id="retype-pwd-txt" required="" class="form-control" maxlength="20"/>
				<span id="retype-password-error" style="color: #C81C1C;"></span>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
  				<button type="submit" class="btn btn-primary" name="reg-submit" id="reg-submit" ><i class="fa fa-key"></i> Create Account</button>
			</div>
		</div>
	 </form>
	 <div class="text-center" style="margin-bottom: 10px;">
	 	<h4>By creating account, you agree to our <a href="termsandconditions.php">Terms & Conditions.</a></h4>
	 </div>
</div>
<script type="text/javascript">
	$(function(){
    	$("#email-error").hide();
		$("#password-error").hide();
		$("#contact-error").hide();
		$("#retype-password-error").hide();

		$("#email-txt").focusout(function(){
			check_email();
		});

		$("#pwd-txt").focusout(function(){
			check_pwd();
		});

		$("#retype-pwd-txt").focusout(function(){
			check_retype_pwd();
		});


		$("#contact-txt").focusout(function(){
			check_contact();
		});



		function check_email(){
				var eml=$("#email-txt").val();
					$.post(
						'register-form-insert.php', 
						{
							postemail:eml
						}, function(data,a,b){
						if(data == "This email is already registered."){
							$("#email-error").html("<i class='fa fa-warning'></i> This email is already registered.");
							$("#email-error").show();
						}else{
							$("#email-error").hide();
						}
					});
				
		}
				
		function check_pwd(){
			var pwd_length=$("#pwd-txt").val().length;
			if(pwd_length<8){
				$("#password-error").html("<i class='fa fa-warning'></i> Password should have atleast 8 characters.");
				$("#password-error").show();
				return false;
			}else{
				$("#password-error").hide();
				return true;
			}
		}


		function check_contact(){
			var contact_length=$("#contact-txt").val().length;
			var Contact=$("#contact-txt").val();
			if(isNaN(Contact) || contact_length < 10){
				$("#contact-error").html("<i class='fa fa-warning'></i> Invalid Contact.");
				$("#contact-error").show();
				return false;
			}else{
				$("#contact-error").hide();
				return true;
			}	
		}

		function check_retype_pwd(){
			var pwd=$("#pwd-txt").val();
			var re_pwd=$("#retype-pwd-txt").val();
			if(pwd != re_pwd){
			$("#retype-password-error").html("<i class='fa fa-warning'></i> Password do not match.");
			$("#retype-password-error").show();
			return false;
			}else{
				$("#retype-password-error").hide();
				return true;
			}
		}

		$("#registration-form").submit(function(event){

			if(check_pwd() == true && check_retype_pwd() == true && check_contact() == true){
			}else{
				event.preventDefault();
			}
	
		});
		document.title = 'Register-RentOnNepal';
	});
</script>
<?php include_once('includes/footer.php');?>