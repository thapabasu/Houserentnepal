<?php  include_once('includes/header.php');?>
  <div style="min-height: 450px;">
    <img src="images/teamlogo.png" width="300px" height="300px" style=" display: block; margin: auto;" /></br>
    <div class="container about-us" style="margin-top: -100px;">
        <h2>हाम्रो बारेमा</h2>
        <p style="font-size: 1.5em;">नमस्कार, हाउस रेन्ट नेपालमा तपाइलाई स्वागत छ। तपाई काठमाडौँ अथवा नेपाल को कुनैपनि ठाउँमा बस्नुहुन्छ र कोठा/फ्ल्याट/घर भाडामा खोज्दै हुनुहुन्छ अथवा भाडामा दिन चाहनुहुन्छ भने हामीलाई सम्झिनुहोस्। हाम्रो लक्ष्य तपाइलाई कोठा/फ्ल्याट/घर भाडामा लिने र दिने कामलाइ सहज र एकदमनै सजिलो बनाउनु हो।</p>
    </div>
  </div>
<?php include_once('includes/footer.php');?>