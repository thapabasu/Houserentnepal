<?php
include_once('includes/header.php');
include_once('db_connect.php');
?>

<?php
if (isset($_POST['paginationgoto'])) {
    if (isset($_POST['paginationinput'])) {
        $type = @$_GET['type'];
        $t = trim($_POST['paginationinput']);
        header("Location: quicksearch.php?page=" . $t . "&type=" . $type);
    }
}
?>

<?php
$type = @$_GET['type'];
$array = explode('on', $type);
$categories = $array[1];
$subCategory = $array[0];
$page = @$_GET['page'];
if ($page == '' || $page == '1') {
    $page1 = 0;
} else {
    $page1 = ($page * 8) - 8;
}
$query = "SELECT `post`.`id`, `post`.`photo`, `post`.`validdate`, `post`.`categories`, `post`.`sub_category`, `post`.`dates`, `post`.`location`, `post`.`title`, `post`.`description`, `post`.`price`, `post`.`post_type`, `users`.`full_name`, `users`.`address`, `users`.`email`, `users`.`contact` FROM `post`, `users` WHERE `users`.`id` = `post`.`user_id` AND `post`.`status` = '1' AND `post`.`categories` = '$categories' AND `post`.`sub_category` = '$subCategory' ORDER BY `post`.`id` DESC LIMIT $page1,8";
$result = mysqli_query($con, $query);
$test = mysqli_num_rows($result);
?>

    <div class="main-body container" style="margin-top: 120px;">
        <?php if ($test == 0){ ?>
            <?php include_once('includes/no-data.php'); ?>
        <?php } else { ?>
        <div class="container">
            <div class="panel-body">
                <button class="btn btn-default" disabled>Filter By</button>
                <div style="display: inline-block;" class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Location
                        <span class="caret"></span>
                    </button>
                    <?php $locationquery = "SELECT DISTINCT `location` FROM `post` WHERE `status` = '1' AND `categories` = '$categories' AND `sub_category` = '$subCategory'" ?>
                    <?php $locationResult = mysqli_query($con, $locationquery); ?>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1"
                        style="max-height: 300px; width: 100%; overflow: scroll;">
                        <?php while ($locationRow = mysqli_fetch_assoc($locationResult)) { ?>
                            <li>
                                <a href="location_search.php?category=<?= $subCategory; ?>&type=<?= $categories ?>&location=<?= $locationRow['location']; ?>"><?php echo $locationRow['location']; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="row">
                    <?php $i = 1;
                    while ($row = mysqli_fetch_assoc($result)) { ?>
                        <!-- col-ms-3 starts starts -->
                        <div class="col-md-3">
                            <!-- panel-default starts -->
                            <div class="panel panel-default">
                                <a
                                        href="" class="quickSearchViewDetail"
                                        data-toggle="modal"
                                        data-target="#viewDetailModal"
                                        data-postImage="<?php echo $row['photo']; ?>"
                                        data-category="<?php echo $row['categories']; ?>"
                                        data-subCategory="<?php echo $row['sub_category']; ?>"
                                        data-postedDate="<?php echo $row['dates']; ?>"
                                        data-postLocation="<?php echo $row['location']; ?>"
                                        data-postTitle="<?= $row['title']; ?>"
                                        data-postPrice="<?= $row['price']; ?>"
                                        data-postType="<?= $row['post_type']; ?>"
                                        data-postDescription="<?= $row['description']; ?>"
                                        data-postValidDate="<?= $row['validdate']; ?>"
                                        data-userName="<?= $row['full_name']; ?>"
                                        data-userAddress="<?= $row['address'] ?>"
                                        data-userEmail="<?= $row['email'] ?>"
                                        data-userContact="<?= $row['contact'] ?>">
                                    <!-- panel-body-img starts -->
                                    <div class="panel-body-img">
                                        <?php if ($row['photo']) { ?>
                                            <img src="./agent/uploads/<?php echo $row['photo']; ?>"
                                                 class="img-responsive img-thumbnail"/>
                                        <?php } else { ?>
                                            <img src="./images/no-photo.jpg" class="img-responsive"/>
                                        <?php } ?>
                                    </div>
                                    <!-- panel-body-img ends -->
                                </a>
                                <!-- panel-body-label starts -->
                                <div class="panel-body-label">
                                    <?php $t = $row['post_type']; ?>
                                    <?php if ($t == "provider") { ?>
                                        <span style="font-family:'Arvo', serif; font-size: 15px;"><?php echo $row['sub_category']; ?>
                                            Available On <?= $row['location'] ?> For <?= $row['categories'] ?></span>
                                        <br/>
                                    <?php } else { ?>
                                        <span style="font-family:'Arvo', serif; font-size: 15px;"><?php echo $row['sub_category']; ?>
                                            Needed On <?= $row['location'] ?> For <?= $row['categories']; ?></span><br/>
                                    <?php } ?>
                                    <a href="" class="quickSearchViewDetail"
                                       data-toggle="modal"
                                       data-target="#viewDetailModal"
                                       data-postId="<?php echo $row['id']; ?>"
                                       data-postImage="<?php echo $row['photo']; ?>"
                                       data-category="<?php echo $row['categories']; ?>"
                                       data-subCategory="<?php echo $row['sub_category']; ?>"
                                       data-postedDate="<?php echo $row['dates']; ?>"
                                       data-postLocation="<?php echo $row['location']; ?>"
                                       data-postTitle="<?= $row['title']; ?>"
                                       data-postPrice="<?= $row['price']; ?>"
                                       data-postType="<?= $row['post_type']; ?>"
                                       data-postDescription="<?= $row['description']; ?>"
                                       data-postValidDate="<?= $row['validdate']; ?>"
                                       data-userName="<?= $row['full_name']; ?>"
                                       data-userAddress="<?= $row['address'] ?>"
                                       data-userEmail="<?= $row['email'] ?>"
                                       data-userContact="<?= $row['contact'] ?>"
                                       style="color: yellow;"><h6>View Details</h6>
                                    </a>
                                </div>
                                <!-- panel-body-label ends -->
                            </div>
                            <!-- panel-default ends -->
                        </div>
                        <!-- col-md-3 ends -->
                        <?php echo($i % 4 == 0 ? '<div class="clearfix"></div>' : ''); ?>
                        <?php $i++;
                    } ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
    </div>

    <div class="clearfix"></div>

    <!-- view detail modal -->
<?php include_once('includes/modal.php'); ?>
    <!-- view detail modal ended -->

    <div class="clearfix"></div>

    <!-- pagination starts -->
    <div class="container-fluid" style="float: right;margin: 3% 8%;">
        <?php $extractdb = mysqli_query($con, "SELECT * FROM `post` WHERE `categories`='$categories' AND `sub_category` = '$subCategory' AND `status`='1'");
        $count = mysqli_num_rows($extractdb);

        $pagedata = ceil($count / 8);
        if ($page == 0) {
            if ($count == 0) {
                $x = 0;
            } else {
                $x = 1;
            }
        } else {
            $x = $page;
        }
        $next = '';
        $previous = '';
        if ($page == '') {
            $next = 2;
        } else {
            $next = $page + 1;
        }
        if ($page > 1) {
            $pre = $page - 1;
        }
        ?>
        <span style="font-style: italic;font-family: Georgia, serif;">Showing <?php echo $x; ?>
            of <?php echo $pagedata; ?></span>

        <?php if ($pagedata > 1 && $pagedata <= 5): ?>
            <?php if ($page == 0 || $page == 1 || $page == 2 || $page == 3 || $page == 4 || $page == 5): ?>
                <?php if ($page != '' && $page != 1): ?>
                    <a href="quicksearch.php?page=<?php echo $pre; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary">Previous</button>
                    </a>
                <?php endif ?>
                <?php for ($a = 1; $a <= $pagedata; $a++) { ?>
                    <a href="quicksearch.php?page=<?php echo $a; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $a; ?>&nbsp;&nbsp;</button>
                    </a>
                <?php } ?>
                <?php if ($page != $pagedata): ?>
                    <a href="quicksearch.php?page=<?php echo $next; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary">Next</button>
                    </a>
                <?php endif ?>
            <?php endif ?>
        <?php endif ?>



        <?php if ($pagedata > 5): ?>
            <?php if ($page == 0 || $page == 1 || $page == 2 || $page == 3): ?>
                <?php if ($page != '' && $page != 1): ?>
                    <a href="quicksearch.php?page=<?php echo $pre; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary">Previous</button>
                    </a>
                <?php endif ?>
                <?php for ($a = 1; $a <= 5; $a++) { ?>
                    <a href="quicksearch.php?page=<?php echo $a; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $a; ?>&nbsp;&nbsp;</button>
                    </a>
                <?php } ?>
                <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                <?php if ($page != $pagedata): ?>
                    <a href="quicksearch.php?page=<?php echo $next; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary">Next</button>
                    </a>
                <?php endif ?>
            <?php endif ?>


            <?php if ($page > 3): ?>
                <?php if ($page <= $pagedata - 2): ?>
                    <?php if ($page != '' && $page != 1): ?>
                        <a href="quicksearch.php?page=<?php echo $pre; ?>&type=<?php echo $type ?>">
                            <button class="btn btn-primary">Previous</button>
                        </a>
                    <?php endif ?>
                    <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                    <?php $var = $page - 2; ?>
                    <?php $variable = $page + 2; ?>
                    <?php for ($value = $var; $value <= $variable; $value++) { ?>
                        <a href="quicksearch.php?page=<?php echo $value; ?>&type=<?php echo $type ?>">
                            <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $value; ?>&nbsp;&nbsp;
                            </button>
                        </a>
                    <?php } ?>
                    <?php if ($page != $pagedata - 2): ?>
                        <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                    <?php endif ?>
                    <?php if ($page != $pagedata): ?>
                        <a href="quicksearch.php?page=<?php echo $next; ?>&type=<?php echo $type ?>">
                            <button class="btn btn-primary">Next</button>
                        </a>
                    <?php endif ?>
                <?php endif ?>
            <?php endif ?>


            <?php if ($page == $pagedata - 1): ?>
                <?php if ($page != '' && $page != 1): ?>
                    <a href="quicksearch.php?page=<?php echo $pre; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary">Previous</button>
                    </a>
                <?php endif ?>
                <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                <?php $q = $page - 3; ?>
                <?php $y = $page + 1; ?>
                <?php for ($p = $q; $p <= $y; $p++) { ?>
                    <a href="quicksearch.php?page=<?php echo $p; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $p; ?>&nbsp;&nbsp;</button>
                    </a>
                <?php } ?>
                <?php if ($page != $pagedata): ?>
                    <a href="quicksearch.php?page=<?php echo $next; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary">Next</button>
                    </a>
                <?php endif ?>
            <?php endif ?>


            <?php if ($page == $pagedata): ?>
                <?php if ($page != '' && $page != 1): ?>
                    <a href="quicksearch.php?page=<?php echo $pre; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary">Previous</button>
                    </a>
                <?php endif ?>
                <button class="btn btn-primary" style="font-weight: bolder;">....</button>
                <?php for ($a = $pagedata - 4; $a <= $pagedata; $a++) { ?>
                    <a href="quicksearch.php?page=<?php echo $a; ?>&type=<?php echo $type ?>">
                        <button class="btn btn-primary text-center">&nbsp;&nbsp;<?php echo $a; ?>&nbsp;&nbsp;</button>
                    </a>
                <?php } ?>
            <?php endif ?>
        <?php endif ?>



        <?php if ($pagedata != '' && $pagedata != 1): ?>
            <div class="pull-right" style="margin: 3px 0 0 2px;">
                <form method="post" action="?type=<?php echo $type ?>">
                    <input type="number" min="1" max="<?php echo $pagedata; ?>" required="" name="paginationinput"/>
                    <input type="submit" value="GO To" name="paginationgoto">
                </form>
            </div>
        <?php endif ?>
    </div>
    <div class="clearfix"></div>
    <!-- pagination ends -->

    <script type="text/javascript">
        $(document).ready(function () {
            $(".homeheader").hide();
            $(".homesecheader").hide();
            $(window).load(function () {
                $(".homeheader").slideDown(1000);
                $(".homesecheader").toggle(2500);
            });
            $(".quickSearchViewDetail").click(function () {
                var postImage = $(this).attr('data-postImage');
                if (postImage == '') {
                    $(".modal-body img").attr("src", './agent/uploads/no-photo.jpg');
                } else {
                    $(".modal-body img").attr("src", './agent/uploads/' + postImage);
                }
                var postCategory = $(this).attr('data-category');
                var postSubCategory = $(this).attr('data-subCategory');
                var postLocation = $(this).attr('data-postLocation');
                var postType = $(this).attr('data-postType');
                if (postType == 'provider') {
                    $(".modal-header h4").html(postSubCategory + ' Available On ' + postLocation + ' For ' + postCategory);
                } else {
                    $(".modal-header h4").html(postSubCategory + ' Needed On ' + postLocation + ' For ' + postCategory);
                }
                var postDate = $(this).attr('data-postedDate');
                $("#postDate").html(postDate);
                var postTitle = $(this).attr('data-postTitle');
                $("#postTitle").html(postTitle);
                var postPrice = $(this).attr('data-postPrice');
                $("#postPrice").html(postPrice);
                var postDescription = $(this).attr('data-postDescription');
                $("#postDescription").html(postDescription);
                var userName = $(this).attr('data-userName');
                $("#userName").html('Name : ' + userName);
                var userAddress = $(this).attr('data-userAddress');
                $("#userAddress").html('Address : ' + userAddress);
                var userContact = $(this).attr('data-userContact');
                $("#userContact").html('Contact : ' + userContact);
                var userEmail = $(this).attr('data-userEmail');
                $("#userEmail").html('Email : ' + userEmail);
                var validdate = $(this).attr('data-postValidDate');
                $("#validDate").html(validdate);
            });
        });
    </script>
<?php include_once('includes/footer.php'); ?>